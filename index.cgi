#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
use strict;

require "CGI.pl";
require "globals.pl";

print "Content-type: text/html\n\n";

PutHeader ("GNOME Bug Tracking System");

print <<FIN;
<table width="100%" cellpadding="10" cellspacing="0">
      <tbody>
         <tr>
      <td colspan="2">                                     
      <center>                                        
      <h2>Welcome to GNOME's Bugzilla Bug Database!</h2>
      </center>
              
      This is the GNOME bug database and tracking system. It is used to 
to  track  bug reports and requests for enhancements for the <a href="http://www.gnome.org/start/stable/">
  GNOME Desktop</a>, and related software such as <a href="http://www.gtk.org">
  GTK+</a>.
      </td>
      </tr>
      <tr>
         <td valign="top" width="50%">                                 
      <h3>New to bugzilla?</h3>
                       
      <ul>
           <li>The <a href="bug-HOWTO.html">bug HOWTO</a> gives you step
by  step instructions for creating your first bugzilla bug.
          <li>There are also <a href="good-bugs.html">bug dos and donts</a>
   that can help you understand the difference between a good bug and a bad
 one, quickly.</li>
          <li>Finally, the <a href="http://mozilla.org">Mozilla</a> <a href="http://www.mozilla.org/bugs">
   bug pages</a> will tell you everything you want to know about Bugzilla,
 including information on how to set one up yourself.       </li>
               
      </ul>
                        
      <h3>To create and manage your account</h3>
                       
      <ul>
          <li><a href="createaccount.cgi">Open a new Bugzilla account</a><br>
          </li>
          <li><a href="relogin.cgi">Forget the currently stored login</a><br>
          </li>
          <li><a href="userprefs.cgi">Change password or user preferences</a><br>
         </li>
               
      </ul>

      <h3>To see the current state of bugzilla</h3>
      <ul>
      <li><a href="mostfrequent.html">The most frequently reported bugs</a> in Bugzilla.<br></li>
      <li><a href="gnome-24-report.html">GNOME 2.4</a> bugs, as indicated by the GNOMEVER2.4 keyword.<br></li>
      <li><a href="gnome-25-report.html">GNOME 2.5</a> bugs, as indicated by the GNOMEVER2.5 keyword.<br></li>
      <li><a href="weekly-bug-summary.html">The past week's</a> bug activity.<br></li>
      <li><a href="reports.cgi">More reports</a> than you can shake a stick at.<br></li>
      </ul>
      
      </td>
      <td width="50%">                         
      <h3>To report a bug</h3>
                       
      <ul>
          <li>Our <a href="simple-bug-guide.cgi">simple bug assistant</a>
  makes it easy to submit a bug report, even if you've never used bugzilla
 before, or are simply in a hurry<b><font color="red">(New!)</font></b>
          </li>
          <li>The <a href="enter_bug.cgi">traditional and more sophisticated
 bug interface</a> is available for people already familiar with bugzilla.
        </li>
               
      </ul>

      <h3>To search for existing bugs</h3>
                                         
      <ul>
          <li><a href="query.cgi">Search the bug database</a><br>
          </li>
          <li>                      
          <form method="get" action="show_bug.cgi">
  bug #<input name="id" size="6"><input type="submit" value="Find"></form>
          </li>
      </ul>
                        
      <h3>To help hunt for bugs</h3>
         The GNOME bug team needs all the help it can get in processing and
 triaging new bugs. Helping with bugs
is  a great way to help make GNOME a better product, even if you have very
little free time or can't program. The following things can help you
get started:
	<ul>
	<li>The <a href="http://developer.gnome.org/projects/bugsquad/">GNOME bugsquad homepage</a> is a good reference for all bug hunting activity.  Among other things it contains the essential <a href="http://developer.gnome.org/projects/bugsquad/triage/">triage guide</a>.
	<li><a href="irc://irc.gnome.org/#bugs">irc.gnome.org #bugs</a> is where the GNOME bug team meets on irc. Find us there to ask questions or let us know you want to help out.
You can also join the <a href="http://mail.gnome.org/mailman/listinfo/gnome-bugsquad">gnome-bugsquad</a> mailing list.

         <li><a href="http://bugzilla.gnome.org/buglist.cgi?bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=NEEDINFO&amp;bug_status=REOPENED&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;email1=&amp;emailtype1=substring&amp;emailassigned_to1=1&amp;email2=&amp;emailtype2=substring&amp;emailreporter2=1&amp;changedin=0&amp;chfield=%5BBug+creation%5D&amp;chfieldfrom=&amp;chfieldto=Now&amp;chfieldvalue=&amp;short_desc=&amp;short_desc_type=substring&amp;long_desc=&amp;long_desc_type=substring&amp;bug_file_loc=&amp;bug_file_loc_type=substring&amp;status_whiteboard=&amp;status_whiteboard_type=substring&amp;keywords=&amp;keywords_type=anywords&amp;op_sys_details=&amp;op_sys_details_type=substring&amp;version_details=&amp;version_details_type=substring&amp;cmdtype=doit&amp;order=Reuse+same+sort+as+last+time&amp;form_name=query">The list of bugs filed today</a> is a good starting place once you've spoken to folks and decided to help out.
      </ul>
                                          
         </td>
      </tr>
                     
  </tbody>     
</table>
FIN
PutFooter();

exit;

