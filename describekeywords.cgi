#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is the Bugzilla Bug Tracking System.
#
# The Initial Developer of the Original Code is Terry Weissman.
# Portions created by Terry Weissman are
# Copyright (C) 2000 Terry Weissman. All
# Rights Reserved.
#
# Contributor(s): Terry Weissman <terry@mozilla.org>

use diagnostics;
use strict;

require "CGI.pl";

ConnectToDatabase();

print "Content-type: text/html\n\n";

PutHeader("Bugzilla keyword description");

my $tableheader = qq{
<TABLE BORDER=1 CELLPADDING=4 CELLSPACING=0>
<TR BGCOLOR="#6666FF">
<TH ALIGN="left">Name</TH>
<TH ALIGN="left">Description</TH>
<TH ALIGN="left">Bugs</TH>
</TR>
};

print $tableheader;
my $line_count = 0;
my $max_table_size = 50;

SendSQL("SELECT keyworddefs.name, keyworddefs.description, 
                COUNT(keywords.bug_id), keywords.bug_id
         FROM keyworddefs LEFT JOIN keywords ON keyworddefs.id=keywords.keywordid, bugs
	 WHERE keywords.bug_id = bugs.bug_id
	 AND (bugs.bug_status = 'NEW' or bugs.bug_status = 'ASSIGNED' or bugs.bug_status = 'REOPENED' or bugs.bug_status = 'UNCONFIRMED')

         GROUP BY keyworddefs.id
         ORDER BY keyworddefs.name");

while (MoreSQLData()) {
    my ($name, $description, $bugs, $onebug) = FetchSQLData();
    if ($bugs && $onebug) {
        # This 'onebug' stuff is silly hackery for old versions of
        # MySQL that seem to return a count() of 1 even if there are
        # no matching.  So, we ask for an actual bug number.  If it
        # can't find any bugs that match the keyword, then we set the
        # count to be zero, ignoring what it had responded.
        my $q = url_quote($name);
        $bugs = qq{<A HREF="buglist.cgi?keywords=$q&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED">$bugs</A>};
    } else {
        $bugs = "none";
    }
    if ($line_count == $max_table_size) {
        print "</table>\n$tableheader";
        $line_count = 0;
    }
    $line_count++;
    print qq{
<TR>
<TH>$name</TH>
<TD>$description</TD>
<TD ALIGN="right">$bugs</TD>
</TR>
};
}

print "</TABLE><P>\n";

quietly_check_login();

if (UserInGroup("editkeywords")) {
    print "<p><a href=editkeywords.cgi>Edit keywords</a><p>\n";
}

PutFooter();
