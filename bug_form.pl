# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is the Bugzilla Bug Tracking System.
#
# The Initial Developer of the Original Code is Netscape Communications
# Corporation. Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation. All
# Rights Reserved.
#
# Contributor(s): Terry Weissman <terry@mozilla.org>
# 	          David Lawrence <dkl@redhat.com>
use diagnostics;
use strict;

use RelationSet;

# Shut up misguided -w warnings about "used only once".  For some reason,
# "use vars" chokes on me when I try it here.

sub bug_form_pl_sillyness {
    my $zz;
    $zz = %::FORM;
    $zz = %::components;
    $zz = %::prodmaxvotes;
    $zz = %::versions;
    $zz = @::legal_keywords;
    $zz = @::legal_opsys;
    $zz = @::legal_product;
    $zz = @::legal_priority;
    $zz = @::legal_resolution_no_dup;
    $zz = @::legal_severity;
    $zz = %::target_milestone;
}

my %bug_form;			# hash to hold variables passed on to the template
my $loginok = quietly_check_login();

my $id = $::FORM{'id'};
$bug_form{id} = $id;

my $query = "
select
        bugs.bug_id,
        product,
        version,
        version_details,
        op_sys,
        op_sys_details,
        bug_status,
        resolution,
        priority,
        bug_severity,
        component,
        assigned_to,
        reporter,
        bug_file_loc,
        short_desc,
	target_milestone,
	qa_contact,
	status_whiteboard,
        date_format(creation_ts,'%Y-%m-%d %H:%i'),
        groupset,
	delta_ts,
	sum(votes.count)
from bugs left join votes using(bug_id)
where bugs.bug_id = $id
and bugs.groupset & $::usergroupset = bugs.groupset
group by bugs.bug_id";

SendSQL($query);
my %bug;
my @row;
if (@row = FetchSQLData()) {
    my $count = 0;
    foreach my $field ("bug_id", "product", "version", "version_details",
		       "op_sys", "op_sys_details", "bug_status", "resolution", "priority",
		       "bug_severity", "component", "assigned_to", "reporter",
		       "bug_file_loc", "short_desc", "target_milestone",
                       "qa_contact", "status_whiteboard", "creation_ts",
                       "groupset", "delta_ts", "votes") {
	$bug{$field} = shift @row;
	if (!defined $bug{$field}) {
	    $bug{$field} = "";
	}
	$count++;
    }
} else {
    SendSQL("select groupset from bugs where bug_id = $id");
    if (@row = FetchSQLData()) {
        print "<H1>Permission denied.</H1>\n";
        if ($loginok) {
            print "Sorry; you do not have the permissions necessary to see\n";
            print "bug $id.\n";
        } else {
            print "Sorry; bug $id can only be viewed when logged\n";
            print "into an account with the appropriate permissions.  To\n";
            print "see this bug, you must first\n";
            print "<a href=\"show_bug.cgi?id=$id&GoAheadAndLogIn=1\">";
            print "log in</a>.";
        }
    } else {
        print "<H1>Bug not found</H1>\n";
        print "There does not seem to be a bug numbered $id.\n";
    }
    PutFooter();
    exit;
}

my $assignedtoid = $bug{'assigned_to'};
my $reporterid = $bug{'reporter'};
my $qacontactid =  $bug{'qa_contact'};

$bug{'assigned_to'} = DBID_to_name($assignedtoid);
$bug{'reporter'} = DBID_to_name($reporterid);
$bug{'assigned_to_name'} = DBID_to_real_or_loginname($assignedtoid);
$bug{'reporter_name'} = DBID_to_real_or_loginname($reporterid);


print qq{<FORM NAME="changeform" METHOD="POST" ACTION="process_bug.cgi">\n};

#  foreach my $i (sort(keys(%bug))) {
#      my $q = value_quote($bug{$i});
#      print qq{<INPUT TYPE="HIDDEN" NAME="orig-$i" VALUE="$q">\n};
#  }

$bug_form{long_desc} = GetLongDescriptionAsHTML($id);
$bug_form{longdesclength} = length($bug_form{long_desc});

GetVersionTable();



#
# These should be read from the database ...
#

$bug_form{'priority_popup'} = "<SELECT NAME=priority>\n" . 
                          make_options(\@::legal_priority, $bug{'priority'}) . 
                          "\n</SELECT>\n";

$bug_form{'severity_popup'} = "<SELECT NAME=bug_severity>\n" .
                               make_options(\@::legal_severity, $bug{'bug_severity'}) .
                               "\n</SELECT>\n";



$bug_form{'component_popup'} = "<B><A HREF=\"describecomponents.cgi?product=" .
								   url_quote($bug{'product'}) . "\">Component:</A></B>" .
                                   "<BR><B>$bug{'component'}</B></TD>\n<TD ROWSPAN=2 VALIGN=top>" .
                                   make_popup('component', $::components{$bug{'product'}}, $bug{'component'}, 1, 0);


my $ccSet = new RelationSet;
$ccSet->mergeFromDB("select who from cc where bug_id=$id");
$bug_form{cc_element} = 'Cc:</TH><TD ALIGN=left><INPUT NAME=cc SIZE=60 VALUE="' .
                        $ccSet->toString() . '">';


$bug_form{'version_popup'} = "<SELECT NAME=version>\n" .
                             make_options($::versions{$bug{'product'}}, $bug{'version'}) .
                             "\n</SELECT>\n";

$bug_form{'product_popup'} = "<SELECT NAME=product>\n" .
                              make_options(\@::legal_product, $bug{'product'}) .
                             "\n</SELECT>\n";

$bug_form{opsys_popup} = "<SELECT NAME=op_sys>\n" .
                         make_options(\@::legal_opsys, $bug{'op_sys'}) .
  			"</SELECT>\n";

$bug_form{os_details_element} = "<INPUT NAME=op_sys_details VALUE=\"" .									 value_quote($bug{'op_sys_details'}) . "\" SIZE=40>";

$bug_form{version_details_element} = "<INPUT NAME=version_details VALUE=\"" .									 value_quote($bug{'version_details'}) . "\" SIZE=40>";

my $resolution_popup = make_options(\@::legal_resolution_no_dup,
                                    $bug{'resolution'});

my $URL = $bug{'bug_file_loc'};

if (defined $URL && $URL ne "none" && $URL ne "NULL" && $URL ne "") {
    $URL = "<B><A HREF=\"$URL\">URL:</A></B>";
} else {
    $URL = "<B>URL:</B>";
}

if (Param("usetargetmilestone")) {
    my $url = "";
    if (defined $::milestoneurl{$bug{'product'}}) {
        $url = $::milestoneurl{$bug{'product'}};
    }
    if ($url eq "") {
        $url = "notargetmilestone.html";
    }
    if ($bug{'target_milestone'} eq "") {
        $bug{'target_milestone'} = " ";
    }
    $bug_form{milestone_popup} = qq{
        <A href=\"$url\"><B>Target<BR>Milestone:</B></A></TD>
            <TD><SELECT NAME=target_milestone>} .
                make_options($::target_milestone{$bug{'product'}},
                             $bug{'target_milestone'}) .
                                 "</SELECT>\n";
} else {
    $bug_form{milestone_popup} = '&nbsp;</TD><TD>&nbsp;';
}

if (Param("useqacontact")) {
    my $name = $bug{'qa_contact'} > 0 ? DBID_to_name($bug{'qa_contact'}) : "";
    $bug_form{qacontact_element} = "

QA Contact:</TH><TD ALIGN=left><INPUT NAME=qa_contact VALUE=\"" .
    value_quote($name) .
    "\" SIZE=60>";
}

$bug_form{url_element} = "$URL</TH><TD ALIGN=left><INPUT NAME=bug_file_loc VALUE=\"$bug{'bug_file_loc'}\" SIZE=60>";

$bug_form{summary_element} = "Summary:</TH><TD COLSPAN=2 ALIGN=left><INPUT NAME=short_desc VALUE=\"" .									 value_quote($bug{'short_desc'}) . "\" SIZE=60>";

if (Param("usestatuswhiteboard")) {
  $bug_form{'whiteboard_element'} = "Status Whiteboard:</TH>" . 
	 		"<TD ALIGN=left><INPUT NAME=status_whiteboard VALUE=\"" .
			value_quote($bug{'status_whiteboard'}) . "\" SIZE=60>";
}

if (@::legal_keywords) {
    SendSQL("SELECT keyworddefs.name 
             FROM keyworddefs, keywords
             WHERE keywords.bug_id = $id AND keyworddefs.id = keywords.keywordid
             ORDER BY keyworddefs.name");
    my @list;
    while (MoreSQLData()) {
        push(@list, FetchOneColumn());
    }
    my $value = value_quote(join(', ', @list));
    $bug_form{keywords_element} = qq{
	<A HREF="describekeywords.cgi">Keywords</A>:</TH>
	<TD ALIGN=left><INPUT NAME="keywords" VALUE="$value" SIZE=60>\n};
}

$bug_form{attachment_element} = "<TABLE WIDTH=\"100%\">\n";

SendSQL("select attach_id, creation_ts, description from attachments where bug_id = $id");
while (MoreSQLData()) {
    my ($attachid, $date, $desc) = (FetchSQLData());
    if ($date =~ /^(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/) {
        $date = "$3/$4/$2 $5:$6";
    }
    my $link = "showattachment.cgi?attach_id=$attachid";
    $desc = value_quote($desc);
    $bug_form{attachment_element} .= qq{
         <tr>    
             <td><a href="$link">$date</a></td><td>$desc</td>
         </tr>
    };
}

$bug_form{attachment_element} .= qq{
     <TR>
	<TD COLSPAN=6><A HREF="createattachment.cgi?id=$id"> 
	Create a new attachment</A> (proposed patch, testcase, etc.)</TD>
    </TR>
};

$bug_form{attachment_element} .= "</TABLE>\n";

sub EmitDependList {
    my ($desc, $myfield, $targetfield) = (@_);
    my $depends = "<td align=right>$desc:</td><td>";
    my @list;
    SendSQL("select dependencies.$targetfield, bugs.bug_status
 from dependencies, bugs
 where dependencies.$myfield = $id
   and bugs.bug_id = dependencies.$targetfield
 order by dependencies.$targetfield");
    while (MoreSQLData()) {
        my ($i, $stat) = (FetchSQLData());
        push(@list, $i);
        my $opened = ($stat eq "NEW" || $stat eq "ASSIGNED" ||
                      $stat eq "REOPENED");
        if (!$opened) {
            $depends .= "<strike>";
        }
        $depends .= qq{<a href="show_bug.cgi?id=$i">$i</a>};
        if (!$opened) {
            $depends .= "</strike>";
        }
        $depends .= " ";
    }
    $depends .= "</td><td><input name=$targetfield value=\"" .
        join(',', @list) . "\"></td>\n";
    return $depends; 
}

if (Param("usedependencies")) {
    $bug_form{depends_element} = "<TABLE><TR>\n<TH ALIGN=right>Dependencies:</TH>\n";
    $bug_form{depends_element} .= EmitDependList("Bug $id depends on", "blocked", "dependson");
    $bug_form{depends_element} .= qq{
<td rowspan=2><a href="showdependencytree.cgi?id=$id">Show dependency tree</a>
};
    if (Param("webdotbase") ne "") {
        $bug_form{depends_element} .= qq{
<br><a href="showdependencygraph.cgi?id=$id">Show dependency graph</a>
};
    }
    $bug_form{depends_element} .= "</td></tr><tr><td></td>";
    $bug_form{depends_element} .= EmitDependList("Bug $id blocks", "dependson", "blocked");
    $bug_form{depends_element} .= "</tr></table>\n";
}

if ($::prodmaxvotes{$bug{'product'}}) {
    $bug_form{'votes_element'} = qq{
<table><tr>
<th><a href="votehelp.html">Votes</a> for bug $id:</th><td>
<a href="showvotes.cgi?bug_id=$id">$bug{'votes'}</a>
&nbsp;&nbsp;&nbsp;<a href="showvotes.cgi?voteon=$id">Vote for this bug</a>
</td></tr></table>
};
}

if ($::usergroupset ne '0') {
    $bug_form{visibility_element} = qq{
<TABLE CELLSPACING=0 CELLPADDING=3 BORDER=1 WIDTH=100%>
  <TR BGCOLOR="#CFCFCF">
    <TD ALIGN=left><B>Visible only to:</B>
    <BR>(Must be in group to change)</TD>
  </TR>
  <TR BGCOLOR="#ECECEC">
    <TD>
};
    SendSQL("select bit, description, (bit & $bug{'groupset'} != 0) from groups where bit & $::usergroupset != 0 and isbuggroup != 0 order by bit");
    while (MoreSQLData()) {
        my ($bit, $description, $ison) = (FetchSQLData());
        my $check = $ison ? "CHECKED" : "";
        $bug_form{visibility_element} .= qq{<INPUT TYPE=checkbox NAME="bit-$bit" $check>$description group<br>\n};
    }
    $bug_form{visibility_element} .= qq{
   </TD>
  </TR>
</TABLE>
};
}

# knum is which knob number we're generating, in javascript terms.

my $knum = 1;

my $status = $bug{'bug_status'};

# In the below, if the person hasn't logged in ($::userid == 0), then
# we treat them as if they can do anything.  That's because we don't
# know why they haven't logged in; it may just be because they don't
# use cookies.  Display everything as if they have all the permissions
# in the world; their permissions will get checked when they log in
# and actually try to make the change.

my $canedit = UserInGroup("editbugs") || ($::userid == 0);
my $canconfirm;

$bug_form{'resolution_change'} = qq{
 <TABLE CELLSPACING=0 CELLPADDING=3 BORDER=1 WIDTH=100%>
  <TR BGCOLOR="#CFCFCF">
     <TD ALIGN=left><B>Change State or Resolution</B>
      <BR>(Allowed only by reporter and privileged members)</TD>
 </TR><TR BGCOLOR="#ECECEC">
     <TD ALIGN=left>
     <INPUT TYPE=radio NAME=knob VALUE=none CHECKED>
         Leave as <b>$bug{'bug_status'} $bug{'resolution'}</b><br>
};

if ($status eq $::unconfirmedstate) {
    $canconfirm = UserInGroup("canconfirm") || ($::userid == 0);
    if ($canedit || $canconfirm) {
        $bug_form{resolution_change} .="<INPUT TYPE=radio NAME=knob VALUE=confirm> ";
	$bug_form{resolution_change} .="Confirm bug (change status to <b>NEW</b>)<br>";
        $knum++;
    }
}

if ($status ne 'NEEDINFO') {
     $bug_form{resolution_change} .="<INPUT TYPE=radio NAME=knob VALUE=needinfo> ";
     $bug_form{resolution_change} .="Report needs more information (change status to <b>NEEDINFO</b>)<br>";
     $knum++;
}

if ($canedit || $::userid == $assignedtoid ||
      $::userid == $reporterid || $::userid == $qacontactid) {
    if (IsOpenedState($status)) {
        if ($status ne "ASSIGNED") {
            $bug_form{resolution_change} .="<INPUT TYPE=radio NAME=knob VALUE=accept> ";
            my $extra = "";
            if ($status eq $::unconfirmedstate && ($canconfirm || $canedit)) {
                $extra = "confirm bug, ";
            }
            $bug_form{resolution_change} .="Accept bug (${extra}change status to <b>ASSIGNED</b>)<br>";
            $knum++;
        }
        if ($bug{'resolution'} ne "") {
            $bug_form{resolution_change} .="<INPUT TYPE=radio NAME=knob VALUE=clearresolution>\n";
            $bug_form{resolution_change} .="Clear the resolution (remove the current resolution of\n";
            $bug_form{resolution_change} .="<b>$bug{'resolution'}</b>)<br>\n";
            $knum++;
        }
        $bug_form{resolution_change} .="<INPUT TYPE=radio NAME=knob VALUE=resolve>
        Resolve bug, changing <A HREF=\"bug_status.html\">resolution</A> to
        <SELECT NAME=resolution
          ONCHANGE=\"document.changeform.knob\[$knum\].checked=true\">
          $resolution_popup</SELECT><br>\n";
        $knum++;
        $bug_form{resolution_change} .="<INPUT TYPE=radio NAME=knob VALUE=duplicate>
        Resolve bug, mark it as duplicate of bug # 
        <INPUT NAME=dup_id SIZE=6 ONCHANGE=\"document.changeform.knob\[$knum\].checked=true\"><br>\n";
        $knum++;
        if ( $bug{'assigned_to'} =~ /(.*)\((.*)\)/ ) {
           $bug{'assigned_to'} = $1;
           chop($bug{'assigned_to'});
        }
        my $assign_element = "<INPUT NAME=\"assigned_to\" SIZE=32 ONCHANGE=\"document.changeform.knob\[$knum\].checked=true\" VALUE=\"$bug{'assigned_to'}\">";

        $bug_form{resolution_change} .="<INPUT TYPE=radio NAME=knob VALUE=reassign> 
          <A HREF=\"bug_status.html#assigned_to\">Reassign</A> bug to
          $assign_element
        <br>\n";
        if ($status eq $::unconfirmedstate && ($canconfirm || $canedit)) {
            $bug_form{resolution_change} .="&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE=checkbox NAME=andconfirm> and confirm bug (change status to <b>NEW</b>)<BR>";
        }
        $knum++;
        $bug_form{resolution_change} .="<INPUT TYPE=radio NAME=knob VALUE=reassignbycomponent>
          Reassign bug to owner of selected component<br>\n";
        if ($status eq $::unconfirmedstate && ($canconfirm || $canedit)) {
            $bug_form{resolution_change} .="&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE=checkbox NAME=compconfirm> and confirm bug (change status to <b>NEW</b>)<BR>";
        }
        $knum++;
    } else {
        $bug_form{resolution_change} .="<INPUT TYPE=radio NAME=knob VALUE=reopen> Reopen bug<br>\n";
        $knum++;
        if ($status eq "RESOLVED") {
            $bug_form{resolution_change} .="<INPUT TYPE=radio NAME=knob VALUE=verify>
        Mark bug as <b>VERIFIED</b><br>\n";
            $knum++;
        }
        if ($status ne "CLOSED") {
            $bug_form{resolution_change} .="<INPUT TYPE=radio NAME=knob VALUE=close>
        Mark bug as <b>CLOSED</b><br>\n";
            $knum++;
        }
    }
}
 
$bug_form{'resolution_change'} .= "
        </TD>
    </TR>
    </TABLE>
";

$bug_form{'commit_change'} = qq{
    <TABLE CELLSPACING=0 CELLPADDING=3 BORDER=1>
    <TR BGCOLOR="#CFCFCF">
        <TD ALIGN=left><B>Private Changes</B><BR>
        (Reporter, assigned, or privileged members only)</TD>
    </TR><TR BGCOLOR="#ECECEC">
        <TD ALIGN=center>
        <TABLE> 
        <TR>
            <TD ALIGN=center VALIGN=center>    
            <INPUT TYPE="submit" VALUE="Save Changes">
            </TD><TD ALIGN=center VALIGN=center>
            <INPUT TYPE="reset" VALUE="Reset">
            <INPUT TYPE=hidden name=form_name VALUE=process_bug>
            <INPUT TYPE=hidden NAME=reporter VALUE="$bug{'reporter'}">
            </TD>
        </TR>
    </TABLE>
  </FORM>
};

# We need to throw all remaining %bug variables into %bug_form for display if needed

# Original RH version had this - which is a cross-site-scripting and bug vulnerability. 
# It could be moved up above the intilization of $bug_form, but I think it would
# be better to simply refer to these as $::FORM{} from the template if needed.
#
#foreach my $key (keys %::FORM) {
#    $bug_form{$key} = $::FORM{$key};
#}

foreach my $key (keys %bug) {
    $bug_form{$key} = $bug{$key};
}

# we can now fill in the bug form template
print LoadTemplate('bugform_gnome.tmpl', \%bug_form);
 
# To add back option of editing the long description, insert after the above
# long_list.cgi line:
#  <A HREF=\"edit_desc.cgi?id=$id\">Edit Long Description</A>

navigation_header();

PutFooter();

1;
