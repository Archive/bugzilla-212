#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is the Bugzilla Bug Tracking System.
#
# The Initial Developer of the Original Code is Netscape Communications
# Corporation. Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation. All
# Rights Reserved.
#
# Contributor(s): Harrison Page <harrison@netscape.com>,
# Terry Weissman <terry@mozilla.org>,
# Dawn Endico <endico@mozilla.org>
# Bryce Nesbitt <bryce@nextbus.COM>,
#    Added -All- report, change "nobanner" to "banner" (it is strange to have a
#    list with 2 positive and 1 negative choice), default links on, add show
#    sql comment.
# Joe Robins <jmrobins@tgix.com>,
#    If using the usebuggroups parameter, users shouldn't be able to see
#    reports for products they don't have access to.
# Gervase Markham <gerv@gerv.net> and Adam Spiers <adam@spiers.net>
#    Added ability to chart any combination of resolutions/statuses.
#    Derive the choice of resolutions/statuses from the -All- data file
#    Removed hardcoded order of resolutions/statuses when reading from
#    daily stats file, so now works independently of collectstats.pl
#    version
#    Added image caching by date and datasets
# Myk Melez <myk@mozilla.org):
#    Implemented form field validation and reorganized code.
#
# Luis Villa <louie@ximian.com>:
# basically, this now just queries for changes by contributor


use diagnostics;
use strict;

require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";
use vars qw(@legal_product); # globals from er, globals.pl

my @status = qw (NEW ASSIGNED REOPENED);
my @products = qw (Evolution GtkHtml GAL GdkPixbuf Bonobo); 
my @priorities = qw (Showstopper Major Normal Minor Cosmetic Wishlist);

my %bugs_per_filer;

#Connect to the DB
ConnectToDatabase(1);

# Output appropriate HTTP response headers
print "Content-type: text/html\n";
# Changing attachment to inline to resolve 46897 - zach@zachlipton.com
print "Content-disposition: inline; filename=bugzilla_report.html\n\n";

PutHeader("Contributor Reports");

print <<FIN;
<center>
<h1>
Bugzilla Contributors Report
</h1>
</center>
FIN

    total_new_bugs();
print "<p>";
new_bugs_this_week();
#print "<p>";
#dup_bugs();

PutFooter();

sub sort_by_total_filed{
    $bugs_per_filer{$a} <=> $bugs_per_filer{$b}
}

sub total_new_bugs {

# Build up $query string for bugs that:
# (1) were opened by outside contributors
# (2) were not marked as duplicates 
# (3) if resolved, were resolved as FIXED. 

my $query;

$query = <<FIN;
select 
    profiles.login_name
from 
    bugs, profiles
where
    (
     bugs.bug_status = 'NEW' or
     bugs.bug_status = 'ASSIGNED' or
     bugs.resolution = 'FIXED'
     )     
and
    profiles.userid = bugs.reporter
FIN

# End build up $query string
    SendSQL ($query);
    
    my @filers_list;

    while (my ($filer) = FetchSQLData()) {
        $bugs_per_filer{$filer} ++;

        if($bugs_per_filer{$filer}==1)
        {
            push @filers_list, $filer;
        }
    }

print <<FIN;
    <h1>Bugs filed</h1>
        <small>Includes all bugs reported that are currently NEW, ASSIGNED, or FIXED.</small>
    <table border=3 cellpadding=5>
    <tr>
    <td align=center bgcolor="#DDDDDD"><b>Reporter</b></td>
    <td align=center bgcolor="#DDDDDD"><b>Bugs Reported</b></td>
    </tr>
FIN


#here we hopefully loop out the table

    @filers_list = reverse sort sort_by_total_filed @filers_list;

    foreach my $filer (@filers_list) {
        if( $bugs_per_filer{$filer} >= 5 )
        {
            print <<FIN;
     <tr><td align=left><tt>$filer</tt></td>
     <td align=center>$bugs_per_filer{$filer}</td></tr>
FIN
}
    }
print <<FIN;
</table>
FIN
}

sub new_bugs_this_week {

# Build up $query string for bugs that:
# (1) were opened by outside contributors
# (2) were not marked as duplicates 
# (3) if resolved, were resolved as FIXED. 

my $query;

$query = <<FIN;
select 
    profiles.login_name
from 
    bugs, profiles
where
    (
     bugs.bug_status = 'NEW' or
     bugs.bug_status = 'ASSIGNED' or
     bugs.resolution = 'FIXED'
     )     
and
    profiles.userid = bugs.reporter
and
    to_days(bugs.creation_ts) >= (to_days(now()) - 7)

FIN

# End build up $query string
    SendSQL ($query);
    
    my @filers_list;
    undef(%bugs_per_filer);

    while (my ($filer) = FetchSQLData()) {
        $bugs_per_filer{$filer} ++;

        if($bugs_per_filer{$filer}==1)
        {
            push @filers_list, $filer;
        }
    }

print <<FIN;
    <h1>Bugs Filed This Week</h1>
        <small>Includes all bugs reported that are currently NEW, ASSIGNED, or FIXED.</small>
    <table border=3 cellpadding=5>
    <tr>
    <td align=center bgcolor="#DDDDDD"><b>Reporter</b></td>
    <td align=center bgcolor="#DDDDDD"><b>Bugs Reported</b></td>
    </tr>
FIN


#here we hopefully loop out the table

    @filers_list = reverse sort sort_by_total_filed @filers_list;

    foreach my $filer (@filers_list) {
        if( $bugs_per_filer{$filer} >= 3) #possibly, this would be better off in the SQL
        {
            print <<FIN;
     <tr><td align=left><tt>$filer</tt></td>
     <td align=center>$bugs_per_filer{$filer}</td></tr>
FIN
     }
    }
print <<FIN;
</table>
FIN
}


