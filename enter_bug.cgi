#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is the Bugzilla Bug Tracking System.
# 
# The Initial Developer of the Original Code is Netscape Communications
# Corporation. Portions created by Netscape are Copyright (C) 1998
# Netscape Communications Corporation. All Rights Reserved.
# 
# Contributor(s): Terry Weissman <terry@mozilla.org>
#                 Dave Miller <dave@intrec.com>
#                 Joe Robins <jmrobins@tgix.com>

########################################################################
#
# enter_bug.cgi
# -------------
# Displays bug entry form. Bug fields are specified through popup menus, 
# drop-down lists, or text fields. Default for these values can be passed
# in as parameters to the cgi.
#
########################################################################

use diagnostics;
use strict;

require "CGI.pl";

# Shut up misguided -w warnings about "used only once".  "use vars" just
# doesn't work for me.

sub sillyness {
    my $zz;
    $zz = $::unconfirmedstate;
    $zz = @::legal_opsys;
    $zz = @::legal_priority;
    $zz = @::legal_severity;
}

# hash to hold values to be passed to the html fill in template
my %enter_bug;

# I've moved the call to confirm_login up to here, since if we're using bug
# groups to restrict bug entry, we need to know who the user is right from
# the start.  If that parameter is turned off, there's still no harm done in
# doing it now instead of a bit later.  -JMR, 2/18/00
# Except that it will cause people without cookies enabled to have to log
# in an extra time.  Only do it here if we really need to.  -terry, 3/10/00
if (Param("usebuggroupsentry")) {
    confirm_login();
}

if (!defined $::FORM{'product'}) {
    GetVersionTable();
    my @prodlist;
    foreach my $p (sort(keys %::versions)) {
        if (defined $::proddesc{$p} && $::proddesc{$p} eq '0') {
            # Special hack.  If we stuffed a "0" into proddesc, that means
            # that disallownew was set for this bug, and so we don't want
            # to allow people to specify that product here.
            next;
        }
        if(Param("usebuggroupsentry")
           && GroupExists($p)
           && !UserInGroup($p)) {
          # If we're using bug groups to restrict entry on products, and
          # this product has a bug group, and the user is not in that
          # group, we don't want to include that product in this list.
          next;
        }
        push(@prodlist, $p);
    }
    if (1 != @prodlist) {
        print "Content-type: text/html\n\n";
        PutHeader("Enter Bug");
        
        print "<H2>First, you must pick a product on which to enter\n";
        print "a bug.</H2>\n";
        print "<table>";
        foreach my $p (sort { uc($a) cmp uc($b) } @prodlist) {
            if (defined $::proddesc{$p} && $::proddesc{$p} eq '0') {
                # Special hack.  If we stuffed a "0" into proddesc, that means
                # that disallownew was set for this bug, and so we don't want
                # to allow people to specify that product here.
                next;
            }
            if(Param("usebuggroupsentry")
               && GroupExists($p)
               && !UserInGroup($p)) {
                # If we're using bug groups to restrict entry on products, and
                # this product has a bug group, and the user is not in that
                # group, we don't want to include that product in this list.
                next;
            }
            print "<tr><th align=right valign=top><a href=\"enter_bug.cgi?product=" . url_quote($p) . "\">$p</a>:</th>\n";
            if (defined $::proddesc{$p}) {
                print "<td valign=top>$::proddesc{$p}</td>\n";
            }
            print "</tr>";
        }
        print "</table>\n";
        PutFooter();
        exit;
    }
    $::FORM{'product'} = $prodlist[0];
}

my $product = $::FORM{'product'};
$enter_bug{product} = $product;

confirm_login();

print "Content-type: text/html\n\n";

sub formvalue {
    my ($name, $default) = (@_);
    if (exists $::FORM{$name}) {
        return $::FORM{$name};
    }
    if (defined $default) {
        return $default;
    }
    return "";
}

sub pickversion {
    my $version = formvalue('version');

    if ( Param('usebrowserinfo') ) {
        if ($version eq "") {
            if ($ENV{'HTTP_USER_AGENT'} =~ m@Mozilla[ /]([^ ]*)@) {
                $version = $1;
            }
        }
    }
    
    if (lsearch($::versions{$product}, $version) >= 0) {
        return $version;
    } else {
        if (defined $::COOKIE{"VERSION-$product"}) {
            if (lsearch($::versions{$product},
                        $::COOKIE{"VERSION-$product"}) >= 0) {
                return $::COOKIE{"VERSION-$product"};
            }
        }
    }
    return $::versions{$product}->[0];
}


sub pickcomponent {
    my $result =formvalue('component');
    if ($result ne "" && lsearch($::components{$product}, $result) < 0) {
        $result = "";
    }
    return $result;
}


sub pickos {
    if (formvalue('op_sys') ne "") {
        return formvalue('op_sys');
    }
    if ( Param('usebrowserinfo') ) {
        for ($ENV{'HTTP_USER_AGENT'}) {
            /Mozilla.*\(.*;.*; IRIX.*\)/    && do {return "IRIX";};
            /Mozilla.*\(.*;.*; 32bit.*\)/   && do {return "Windows";};
            /Mozilla.*\(.*;.*; 16bit.*\)/   && do {return "Windows";};
            /Mozilla.*\(.*;.*; 68K.*\)/     && do {return "Macintosh";};
            /Mozilla.*\(.*;.*; PPC.*\)/     && do {return "Macintosh";};
            /Mozilla.*\(.*;.*; OSF.*\)/     && do {return "OSF/1";};
            /Mozilla.*\(.*;.*; Linux.*\)/   && do {return "Linux";};
            /Mozilla.*\(.*;.*; SunOS 5.*\)/ && do {return "Solaris";};
            /Mozilla.*\(.*;.*; SunOS.*\)/   && do {return "SunOS";};
            /Mozilla.*\(.*;.*; SunOS.*\)/   && do {return "SunOS";};
            /Mozilla.*\(.*;.*; BSD\/OS.*\)/ && do {return "BSDI";};
            /Mozilla.*\(Win16.*\)/          && do {return "Windows";};
            /Mozilla.*\(Win95.*\)/          && do {return "Windows";};
            /Mozilla.*\(Win98.*\)/          && do {return "Windows";};
            /Mozilla.*\(WinNT.*\)/          && do {return "Windows";};
            /Mozilla.*\(Windows.*NT/        && do {return "Windows";};
            /Mozilla.*Windows NT 5.*\)/     && do {return "Windows";};
        }
    }
    # default
    return "other";
}

GetVersionTable();

$enter_bug{'os_details_element'} = "<A HREF=\"bug_status.html#op_sys_details\"><B>OS Details:</B></A></TD><TD COLSPAN=3>" . 
							   GeneratePeopleInput('op_sys_details', formvalue('op_sys_details'));

$enter_bug{'assign_element'} = "<A HREF=\"bug_status.html#assigned_to\"><B>Assigned To:</B></A></TD><TD>" . 
							   GeneratePeopleInput('assigned_to', formvalue('assigned_to'));

$enter_bug{'cc_element'} = "<B>Cc:</B></TD><TD>" .
						   GeneratePeopleInput('cc', formvalue('cc'));

my $priority = Param('defaultpriority');

$enter_bug{'severity_popup'} = "<A HREF=\"bug_status.html#severity\"><B>Severity:</B></A></TD><TD>" . 
							   make_popup('bug_severity', \@::legal_severity,
                           	   formvalue('bug_severity', 'normal'), 0);

$enter_bug{'opsys_popup'} = "<A HREF=\"bug_status.html#op_sys\"><B>OS:</B></A></TD><TD>" . 
							make_popup('op_sys', \@::legal_opsys, pickos(), 0);

if ($::components{$product} == "") { # No components
      print "<H1>No components.</H1>\n";
      print "The selected product has no components. At least one\n";
      print "component must be created before bugs can be filed\n";
      print "against this product.\n";
      print "<P>\n";
      PutFooter();

      exit;
      
} elsif (1 == @{$::components{$product}}) {
    # Only one component; just pick it.
    $::FORM{'component'} = $::components{$product}->[0];
}

$enter_bug{'component_popup'} = "<A HREF=\"describecomponents.cgi?product=" . url_quote($product) . "\">" .
								"<B>Component:</B></A></TD><TD>" . 
								make_popup('component', $::components{$product},
                                formvalue('component'), 1);

PutHeader ("Enter Bug","Enter Bug","This page lets you enter a new bug into Bugzilla.");

# Modified, -JMR, 2/24,00
# If the usebuggroupsentry parameter is set, we need to check and make sure
# that the user has permission to enter a bug against this product.
# Modified, -DDM, 3/11/00
# added GroupExists check so we don't choke on a groupless product
if(Param("usebuggroupsentry")
   && GroupExists($product)
   && !UserInGroup($product)) {
  print "<H1>Permission denied.</H1>\n";
  print "Sorry; you do not have the permissions necessary to enter\n";
  print "a bug against this product.\n";
  print "<P>\n";
  PutFooter();
  exit;
}

# Modified, -JMR, 2/18/00
# I'm putting in a select box in order to select whether to restrict this bug to
# the product's bug group or not, if the usebuggroups parameter is set, and if
# this product has a bug group.  This box will default to selected, but can be
# turned off if this bug should be world-viewable for some reason.
#
# To do this, I need to (1) get the bit and description for the bug group from
# the database, (2) insert the select box in the giant print statements below,
# and (3) update post_bug.cgi to process the additional input field.

# Modified, -DDM, 3/11/00
# Only need the bit here, and not the description.  Description is gotten
# when the select boxes for all the groups this user has access to are read
# in later on.
# First we get the bit and description for the group.
my $group_bit=0;
if(Param("usebuggroups") && GroupExists($product)) {
    SendSQL("select bit from groups ".
            "where name = ".SqlQuote($product)." ".
            "and isbuggroup != 0");
    ($group_bit) = FetchSQLData();
}

if (Param("entryheaderhtml")){
	$enter_bug{'entryheaderhtml'} = Param("entryheaderhtml"); 
}

$enter_bug{'version_element'} = "<B>Version:</B></TD><TD>" . 
								Version_element(pickversion(), $product);

$enter_bug{'component_describe'} = url_quote($product);

if (Param('letsubmitterchoosepriority')) {
    $enter_bug{'priority_popup'} = "<B><A HREF=\"bug_status.html#priority\">Priority</A>:</B></TD><TD>" . 
								   make_popup('priority', \@::legal_priority,
    							   formvalue('priority', $priority), 0);
} else {
    $enter_bug{'priority_popup'} = "<INPUT TYPE=HIDDEN NAME=priority VALUE=\"" .
        						   value_quote($priority) . "\"></TD><TD>\n";
}

#replace URL (which is never used) with keyword

$enter_bug{'keyword_element'} = "<B><A HREF=\"describekeywords.cgi\">Keywords</A>:</B></TD><TD>" . 
							GeneratePeopleInput('keywords', formvalue('keywords'));

$enter_bug{'summary_element'} = "<B>Summary:</B></TD><TD>" .
								GeneratePeopleInput('short_desc', formvalue('short_desc'));

$enter_bug{'comment'} = value_quote(formvalue('comment'));

if (UserInGroup("editbugs") || UserInGroup("canconfirm")) {
    SendSQL("SELECT votestoconfirm FROM products WHERE product = " .
            SqlQuote($product));
    
    if (FetchOneColumn()) {
        $enter_bug{'initialstate_popup'} = qq{
    <B><A HREF="bug_status.html#status">Initial state:</B></A></TD>
    <TD>
};
         $enter_bug{'initialstate_popup'} .= BuildPulldown("bug_status",
                            			   [[$::unconfirmedstate], ["NEW"]],
                            			   "NEW");
    }
}

if (!exists $enter_bug{'initialstate_popup'}) {
    $enter_bug{'initialstate_popup'} = "<INPUT TYPE=hidden NAME=bug_status VALUE=\"NEW\">\n";
}

if ($::usergroupset ne '0') {
    $enter_bug{'group_select'} = "
<table>
  <tr>
    <td align=right><B>Access:</td>
    <td colspan=5>";


  
    SendSQL("SELECT bit, description FROM groups " .
            "WHERE bit & $::usergroupset != 0 " .
            "  AND isbuggroup != 0 ORDER BY description");
     while (MoreSQLData()) {
        my ($bit, $description) = (FetchSQLData());
        # Rather than waste time with another Param check and another database
        # access, $group_bit will only have a non-zero value if we're using
        # bug groups and have  one for this product, so I'll check on that
        # instead here.  -JMR, 2/18/00
        # Moved this check to this location to fix conflict with existing
        # select-box patch.  Also, if $group_bit is 0, it won't match the
        # current group, either, so I'll compare it to the current bit
        # instead of checking for non-zero. -DDM, 3/11/00
        my $check = ""; # default selection
        if($group_bit == $bit) {
            # In addition, we need to handle the possibility that we're coming
            # from a bookmark template.  We'll simply check if we've got a
            # parameter called bit-# passed.  If so, then we're coming from a
            # template, and we'll use the template value.
            $check = exists $::FORM{"bit-$bit"} ? "CHECKED" : "";
        }
        $enter_bug{group_select} .= qq{<INPUT TYPE=checkbox NAME="bit-$bit" $check>Must be in the \"$description\" group to see this bug<br>\n};

    }

    $enter_bug{'group_select'} .= "
    </td>
  </tr>
</table>";
}


if ( Param('usebrowserinfo') ) {
   $enter_bug{'browserinfo'} = "
     Some fields initialized from your user-agent, 
     <B>$ENV{'HTTP_USER_AGENT'}</B>.  If you think it got it wrong, 
     please file a separate bug report against the bugzilla.gnome.org
     product including the values you think it should have used.
";

}
# Original RH version had this - which is a cross-site-scripting and bug vulnerability. 
# It could be moved up above the intilization of $bug_form, but I think it would
# be better to simply refer to these as $::FORM{} from the template if needed.
#
# foreach my $key (keys %::FORM) {
# 	$enter_bug{$key} = $::FORM{$key};
# }

print LoadTemplate('enterbug_gnome.tmpl', \%enter_bug);

PutFooter();

