#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is the Bugzilla Bug Tracking System.
#
# The Initial Developer of the Original Code is Netscape Communications
# Corporation. Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation. All
# Rights Reserved.
#
# Contributor(s): Terry Weissman <terry@mozilla.org>


# This is a script suitable for running once a day from a cron job. 

# The purpose of the script is to report on the change in the number of GNOME2
# bugs over the course of a day.

use diagnostics;
use strict;

require "globals.pl";

ConnectToDatabase();

my %bugs;

my $date = `date +%D`;

#bugs with gnome2 keyword added
my $query;

$query = <<FIN;
select 
    distinct bugs_activity.bug_id
from 
    bugs_activity
where
    to_days(bugs_activity.bug_when) = (to_days(now()))
and
    bugs_activity.oldvalue NOT LIKE '%GNOME2%'
and
    bugs_activity.newvalue LIKE '%GNOME2%'
FIN

SendSQL($query);

while (my ($bug_id) = FetchSQLData()) {
        $bugs{'GNOME2_added'}++;
}

$bugs{'bug_increase'}+=$bugs{'GNOME2_added'};

#bugs with gnome2 keyword removed
$query = <<FIN;
select 
    distinct bugs_activity.bug_id
from 
    bugs_activity
where
    to_days(bugs_activity.bug_when) = (to_days(now()))
and
    bugs_activity.oldvalue LIKE '%GNOME2%'
and
    bugs_activity.newvalue NOT LIKE '%GNOME2%'
FIN

SendSQL($query);

while (my ($bug_id) = FetchSQLData()) {
        $bugs{'GNOME2_removed'}++;
}

$bugs{'bug_decrease'}+=$bugs{'GNOME2_removed'};

#bugs with gnome2 that have been created today
$query = <<FIN;
select 
    distinct bugs.bug_id
from 
    bugs
where
    to_days(bugs.creation_ts) = (to_days(now()))
and
    bugs.keywords LIKE '%GNOME2%'
FIN

SendSQL($query);

while (my ($bug_id) = FetchSQLData()) {
        $bugs{'created_with_keyword'}++;
}

$bugs{'bug_increase'}+=$bugs{'created_with_keyword'};

#bugs with gnome2 that have been closed today
$query = <<FIN;
select 
    distinct bugs.bug_id
from 
    bugs
where
    to_days(bugs.delta_ts) = (to_days(now()))
and
    bugs.keywords LIKE '%GNOME2%'
and
    (
     bugs.resolution = 'FIXED'
     or
     bugs.resolution = 'VERIFIED'
     or
     bugs.resolution = 'CLOSED'
     )
FIN

SendSQL($query);

while (my ($bug_id) = FetchSQLData()) {
        $bugs{'keyword_and_fixed'}++;
}

$bugs{'bug_decrease'}+=$bugs{'keyword_and_fixed'};

#bugs with gnome2 that have been duplicated today
$query = <<FIN;
select 
    distinct bugs.bug_id
from 
    bugs
where
    to_days(bugs.delta_ts) = (to_days(now()))
and
    bugs.keywords LIKE '%GNOME2%'
and
    bugs.resolution = 'DUPLICATE'
FIN

SendSQL($query);

while (my ($bug_id) = FetchSQLData()) {
        $bugs{'keyword_and_dupd'}++;
}

$bugs{'bug_decrease'}+=$bugs{'keyword_and_dupd'};


#bugs with gnome2 that have been NEEDINFOd today
$query = <<FIN;
select 
    distinct bugs.bug_id
from 
    bugs
where
    to_days(bugs.delta_ts) = (to_days(now()))
and
    bugs.keywords LIKE '%GNOME2%'
and
    bugs.bug_status = 'NEEDINFO'
FIN

SendSQL($query);

while (my ($bug_id) = FetchSQLData()) {
        $bugs{'keyword_and_needinfod'}++;
}

$bugs{'bug_decrease'}+=$bugs{'keyword_and_needinfod'};

my $msg;

#really, really simplistic message- should be templatized.
$msg = <<FIN;
From: louie\@ximian.com
To: unknown\@bugzilla.gnome.org
Subject: QA report for $date

GNOME2 bugs opened:
    bugs opened today with GNOME2 keyword: $bugs{'created_with_keyword'}

    bugs with GNOME2 keyword added today: $bugs{'GNOME2_added'}

    total opened: $bugs{'bug_increase'}

GNOME2 bugs closed today:
    bugs NEEDINFOd: $bugs{'keyword_and_needinfod'}
    bugs marked DUPLICATE: $bugs{'keyword_and_dupd'}
    bugs FIXED/VERIFIED/CLOSED: $bugs{'keyword_and_fixed'}
    bugs with GNOME2 keyword removed: $bugs{'GNOME2_removed'}

    total closed: $bugs{'bug_decrease'}

FIN

#    print $msg;
    
    open(SENDMAIL, "|/usr/lib/sendmail -t") || die "Can't open sendmail";
    print SENDMAIL $msg;
    close SENDMAIL;
