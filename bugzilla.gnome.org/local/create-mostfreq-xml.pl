#!/usr/local/perl/bin/perl -w
#
# create-mostfreq-xml.cgi
#
# Written for ximian.bugzilla.org by Luis Villa (louie@ximian.com).
#
# Adapted to gnome.bugzilla.org by Wayne Schuller (k_wayne@linuxpower.org)
#
# Summary: Output an xml file of most frequently reported bugs for bug-buddy.
# This allows bug-buddy users to see if a bug has already been reported.
#
# It simply queries for entries with a particular keyword. These have to 
# be manually marked by the bug-hunters. It is better to do it manually then
# we can manage what the bug-buddy users will see.
#
# The output from this script is not designed for a web browser. It is plain
# xml pumped to STDOUT.
#
#TODO: 
#	- doesn't limit number of bugs per component yet
# 	- Output to seperate xml files for each product.
#	- Use the shadow database?
#	- write a proper DTD

use DBI;
use XML::Generator;
use strict;
use vars (qw[$db_host $db_port $db_user $db_pass]);

do '/home/admin/bugzilla/private.pl' or die "Can't read bugzilla-private.pl: $!";
do '/usr/local/www/bugzilla/bugzilla.gnome.org/localconfig' or die "Can't real localconfig: $!";

my $drh = DBI->install_driver('mysql') or die "Can't connect to the database.";
my $connectstring = "dbi:mysql:bugs:host=$db_host:port=$db_port";
my $dbh = DBI->connect($connectstring, $db_user, $db_pass)
    or die "Can't connect to the table '$connectstring'.";

my $xml = XML::Generator->new('escape' => 'always',
			      'conformance' => 'strict',
			      'pretty' => 2
			      );

my $product = "%"; # equals all products - not very useful because of $limit
my $limit = 500; # Only show the top 500.

&output_most_freq_xml($product, $limit);

sub output_most_freq_xml() {
	my($product, $limit) = @_;
	my $query;
	my $count;

	$query = <<FIN;
select
	bug_id, product, component, short_desc
from 
	bugs
where 
	keywords LIKE '%bugbuddy%'
and
	product LIKE '$product'

order by product, component, creation_ts DESC
limit $limit 
FIN


	# We attempt to build the xml tree from the inside out.
	# I wish XML::Generator did this for us... maybe XML::Parser would...
	# Basically it involves adding bugs to @bugs until we hit a new 
	# component. Then we add that component to @components and go to 
	# the next one. If we hit a new product we add the @components
	# to @products and go again.
	my (@bugs, @components, @products);
	my $oldproduct = "FIRST";
	my $oldcomponent = "FIRST";

	my $sth = $dbh->prepare($query);
	$sth->execute();

	# Loop through the list of bugs returned from the query.
	while (my @row = $sth->fetchrow_array) {
		my $bug_id = $row[0];
		my $product = $row[1];
		my $component = $row[2];
		my $short_desc = $row[3];

		if ($oldproduct ne "FIRST" && $oldproduct ne $product) {

			# New product.
			# Push old bugs into old component.
			# Push old components into old product.
	    		push @components, $xml->component({name => $oldcomponent}, @bugs);
    			push @products, $xml->product({name => $oldproduct}, @components);

			@bugs = (); # Clear bug array.
			@components = (); # Clear component array.

			} elsif ($oldcomponent ne "FIRST" && 
					$oldcomponent ne $component) {

			# New component.
			# Push old bugs into old component.
	    		push @components, $xml->component({name => $oldcomponent}, @bugs);

			@bugs = (); # Clear bug array.
			}

		# Add the next bug to @bugs.
		push @bugs, $xml->bug({ bugid => $bug_id }, $xml->desc($short_desc), $xml->url('http://bugzilla.gnome.org/show_bug.cgi?id='.$bug_id));

		$oldcomponent = $component;
		$oldproduct = $product;
	}

	#  We need to close the current component/product node.
	if ($oldproduct ne "FIRST") {
		push @components, $xml->component({name => $oldcomponent}, @bugs);
		push @products, $xml->product({name => $oldproduct}, @components);
	}

	my $total = $xml->products(@products);
	print qq[<!DOCTYPE products SYSTEM "http://bugzilla.gnome.org/bugzilla.gnome.org/bugzilla-gnome-org.dtd">\n];
	print $total . "\n";
}
