#!/bin/bash
#
# This script is cron'ed to run every Saturday.

BUGZILLA_HOME=/usr/local/www/bugzilla/bugzilla.gnome.org

# Send the Gnome weekly summary report.
(cd $BUGZILLA_HOME && ./email_gnome_summary_report.pl )
