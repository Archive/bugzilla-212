#!/bin/bash
#
# This script is cron'ed to run every 30 minutes.

BUGZILLA_HOME=/usr/local/www/bugzilla/bugzilla.gnome.org

(cd $BUGZILLA_HOME/bugzilla.gnome.org && ./local/create-products-xml.pl > temp.xml && iconv -c -f ASCII -t utf8 temp.xml > temp2.xml && rm temp.xml && mv temp2.xml bugzilla-products.xml)
(cd $BUGZILLA_HOME/bugzilla.gnome.org && xmllint --noout bugzilla-products.xml 2>/dev/null; if [ $? = 1 ]; then xmllint --recover  bugzilla-products.xml > temp.xml 2>/dev/null && mv -f temp.xml bugzilla-products.xml;fi)
(cd $BUGZILLA_HOME/bugzilla.gnome.org && ./local/create-config-xml.pl > temp.xml && iconv -c -f ASCII -t utf8 temp.xml > temp2.xml && rm temp.xml && mv temp2.xml bugzilla-config.xml)
(cd $BUGZILLA_HOME/bugzilla.gnome.org && ./local/create-mostfreq-xml.pl > temp.xml && iconv -c -f ASCII -t utf8 temp.xml > temp2.xml && rm temp.xml && mv temp2.xml bugzilla-mostfreq.xml)
(cd $BUGZILLA_HOME/bugzilla.gnome.org && md5sum bugzilla-products.xml > md5sums)
(cd $BUGZILLA_HOME/bugzilla.gnome.org && md5sum bugzilla-config.xml >> md5sums)
(cd $BUGZILLA_HOME/bugzilla.gnome.org && md5sum bugzilla-mostfreq.xml >> md5sums)

# 2.0.x report
(cd $BUGZILLA_HOME && wget http://bugzilla.gnome.org/gnome-report.cgi?keyword=GNOMEVER2.0 -qO temp2.0.html)
cd $BUGZILLA_HOME
if  test -f temp2.0.html
then mv temp2.0.html gnome-20-report.html
fi

# 2.2.x report
(cd $BUGZILLA_HOME && wget http://bugzilla.gnome.org/gnome-report.cgi?keyword=GNOMEVER2.2 -qO temp2.2.html)
cd $BUGZILLA_HOME
if  test -f temp2.2.html
then mv temp2.2.html gnome-22-report.html
fi

# 2.3.x report
(cd $BUGZILLA_HOME && wget http://bugzilla.gnome.org/gnome-report.cgi?keyword=GNOMEVER2.3 -qO temp2.3.html)
cd $BUGZILLA_HOME
if  test -f temp2.3.html
then mv temp2.3.html gnome-23-report.html
fi

# 2.4.x report
(cd $BUGZILLA_HOME && wget http://bugzilla.gnome.org/gnome-report.cgi?keyword=GNOMEVER2.4 -qO temp2.4.html)
cd $BUGZILLA_HOME
if  test -f temp2.4.html
then mv temp2.4.html gnome-24-report.html
fi

# 2.5.x report
(cd $BUGZILLA_HOME && wget http://bugzilla.gnome.org/gnome-report.cgi?keyword=GNOMEVER2.5 -qO temp2.5.html)
cd $BUGZILLA_HOME
if  test -f temp2.5.html
then mv temp2.5.html gnome-25-report.html
fi

# Cache the weekly bug summaries - Wayne Schuller (Nov 2002).
# We use wget because it is the only way I could get this script
# to pass cgi parameters to the cgi files.
(cd $BUGZILLA_HOME && ./weekly-bug-summary.cgi > tmp-weekly-bug-summary.html)
(cd $BUGZILLA_HOME && wget http://bugzilla.gnome.org/weekly-bug-summary.cgi -qO weekly-bug-summary.html)
cd $BUGZILLA_HOME
if  test -f tmp-weekly-bug-summary.html
then mv tmp-weekly-bug-summary.html weekly-bug-summary.html
fi

# 2.0.x weekly bug summary
(cd $BUGZILLA_HOME && wget http://bugzilla.gnome.org/weekly-bug-summary.cgi?KEYWORD=GNOMEVER2.0 -qO weekly-bug-summary-gnome20.html)
cd $BUGZILLA_HOME
if  test -f tmp-weekly-bug-summary-gnome20.html
then mv tmp-weekly-bug-summary-gnome20.html weekly-bug-summary-gnome20.html
fi

# 2.2.x weekly bug summary
(cd $BUGZILLA_HOME && wget http://bugzilla.gnome.org/weekly-bug-summary.cgi?KEYWORD=GNOMEVER2.2 -qO weekly-bug-summary-gnome22.html)
cd $BUGZILLA_HOME
if  test -f tmp-weekly-bug-summary-gnome22.html
then mv tmp-weekly-bug-summary-gnome22.html weekly-bug-summary-gnome22.html
fi

# 2.3.x weekly bug summary
(cd $BUGZILLA_HOME && wget http://bugzilla.gnome.org/weekly-bug-summary.cgi?KEYWORD=GNOMEVER2.3 -qO weekly-bug-summary-gnome23.html)
cd $BUGZILLA_HOME
if  test -f tmp-weekly-bug-summary-gnome23.html
then mv tmp-weekly-bug-summary-gnome23.html weekly-bug-summary-gnome23.html
fi

# 2.4.x weekly bug summary
(cd $BUGZILLA_HOME && wget http://bugzilla.gnome.org/weekly-bug-summary.cgi?KEYWORD=GNOMEVER2.4 -qO weekly-bug-summary-gnome24.html)
cd $BUGZILLA_HOME
if  test -f tmp-weekly-bug-summary-gnome24.html
then mv tmp-weekly-bug-summary-gnome24.html weekly-bug-summary-gnome24.html
fi

# 2.5.x weekly bug summary
(cd $BUGZILLA_HOME && wget http://bugzilla.gnome.org/weekly-bug-summary.cgi?KEYWORD=GNOMEVER2.5 -qO weekly-bug-summary-gnome25.html)
cd $BUGZILLA_HOME
if  test -f tmp-weekly-bug-summary-gnome25.html
then mv tmp-weekly-bug-summary-gnome25.html weekly-bug-summary-gnome25.html
fi

# mostfrequent.cgi (this script uses lots of CPU)
cd $BUGZILLA_HOME && wget http://bugzilla.gnome.org/mostfrequent.cgi -qO mostfrequent.html
