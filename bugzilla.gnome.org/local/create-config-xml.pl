#!/usr/local/perl/bin/perl -wT

use DBI;
use XML::Generator;
use strict;
use vars (qw[$db_host $db_port $db_user $db_pass @severities @priorities @opsys @platforms]);

my $xml = XML::Generator->new('escape' => 'always',
			      'conformance' => 'strict',
			      'pretty' => 2
			      );

do '/home/admin/bugzilla/private.pl' or die "Can't read bugzilla-private.pl: $!";
do '/usr/local/www/bugzilla/bugzilla.gnome.org/localconfig' or die "Can't real localconfig: $!";

my $drh = DBI->install_driver('mysql') or die "Can't connect to the database.";
my $connectstring = "dbi:mysql:bugs:host=$db_host:port=$db_port";
my $dbh = DBI->connect($connectstring, $db_user, $db_pass)
    or die "Can't connect to the table '$connectstring'.";

my $total = '';
my @total_xml;

{
    my @fields;
    foreach (@severities) {
	push @fields, $xml->severity($_);
    }

    push @total_xml, $xml->severities(@fields);
}

{
    my @fields;
    foreach (@priorities) {
	push @fields, $xml->priority($_);
    }

    push @total_xml, $xml->priorities(@fields);
}

{
    my @fields;
    foreach (@opsys) {
	push @fields, $xml->opsys($_);
    }

    push @total_xml, $xml->opsys_list(@fields);
}

{
    my @fields;
    foreach (@platforms) {
	push @fields, $xml->platform($_);
    }

    push @total_xml, $xml->platforms(@fields);
}

my @bugstates = qw(UNCONFIRMED NEW ASSIGNED NEEDINFO REOPENED RESOLVED VERIFIED CLOSED);
my @resolutions = qw(FIXED WONTFIX LATER REMIND DUPLICATE NOTABUG NOTGNOME INCOMPLETE);

{
    my @fields;
    foreach (@bugstates) {
	push @fields, $xml->bug_state($_);
    }

    push @total_xml, $xml->bug_states(@fields);
}

{
    my @fields;
    foreach (@resolutions) {
	push @fields, $xml->resolution($_);
    }

    push @total_xml, $xml->resolutions(@fields);
}

$total = $xml->localconfig (@total_xml);

print qq[<!DOCTYPE localconfig SYSTEM "http://bugzilla.gnome.org/bugzilla.gnome.org/bugzilla-gnome-org.dtd">\n];
print $total . "\n";

END { $dbh->disconnect if $dbh }


