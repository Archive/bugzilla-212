#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#

#use diagnostics;
#use strict;

require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";

#set up the page
ConnectToDatabase(1);
GetVersionTable();

# Output appropriate HTTP response headers
print "Content-type: text/html\n\n";

#no product? We're on Page One.
if (!defined $product)
{
confirm_login();
my $login_name = $::COOKIE{"Bugzilla_login"};

PutHeader("xine-lib bug reporting",'');

#FIXME: we need to autogenerate the product list, maybe?
print<<FIN;
<center><h3>Submitting a xine-lib Bug Report</h3></center><br>
<center>
A <a href="enter_bug.cgi?product=xine-lib">more sophisticated bug submission interface</a> is also available.
</center>
<br>

<form method=post action="post_bug.cgi">

<input type="hidden" name="reporter" value="$login_name">
<!-- <input type="hidden" name="version" value="unspecified"> -->
<input type="hidden" name="version" value="1-rc2">
<input type="hidden" name="bug_severity" value="normal">
<input type="hidden" name="op_sys" value="All">
<input type="hidden" name="product" value="xine-lib">
<input type="hidden" name="component" value="general">
<input type="hidden" name="priority" value='Normal'>
<input type="hidden" name="form_name" value="enter_bug">

<table><tr><td>
<b>Summary:</b></td><td><input NAME="short_desc" SIZE=45 VALUE=""><p>

<tr><td ALIGN=right VALIGN=top><b>Description:</b></td>
   <td><textarea WRAP=HARD NAME=comment ROWS=25 COLS=75>
Description of problem:

Version number of xine-lib:

Name and version number of the front-end used (if applicable):

How often does it happen?

What are the steps to reproduce the problem?
1.
2.
3.

What are the actual results?

And what are the expected results?

URL (Location) of the crashing file/stream (if applicable):


</textarea></td>
</tr>

<tr><td></td><td align="right">
<input type=submit value="File Bug Report">
</td>

</table>

<!-- <center>
   <input type=submit value="File Bug Report">
</center> -->

</form>

<br>
FIN
}

PutFooter();
