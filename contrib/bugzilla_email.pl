#!/usr/local/perl/bin/perl -wT
# -*- Mode: perl; indent-tabs-mode: nil -*-

# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.

# The purpose of this script is to take an email message, which
# specifies a bugid and append it to the bug as part of the longdesc
# table

# Contributor : Seth M. Landsman <seth@dworkin.net>
#               Martin Baulig <baulig@suse.de>

use diagnostics;
use strict;
use MIME::Parser;
require Mail::Address;
require Mail::Send;

$ENV{PATH} = '/bin:/usr/bin';

push @INC, "../."; # this script lives in contrib
push @INC, ".";
require "globals.pl";
require "BugzillaEmail.pm";
require "Bug.pm";

# Create a new MIME parser:
my $parser = new MIME::Parser;

my $Comment = "";
my $Subject = "(unknown subject)";

my $ErrorExit = 0;
my @Messages;

sub DealWithError($);

# Create and set the output directory:
# FIXME: There should be a $BUGZILLA_HOME variable (SML)
(-d "../data/mimedump-tmp") or mkdir "../data/mimedump-tmp",0755 or die "mkdir: $!";
(-w "../data/mimedump-tmp") or die "can't write to directory";

$parser->output_dir("../data/mimedump-tmp");
    
# Read the MIME message:
my $entity = $parser->read(\*STDIN) or die "couldn't parse MIME stream";
$entity->remove_sig(10);          # Removes the signature in the last 10 lines

$Subject = $entity->get('Subject');
$Subject ||= "(unknown subject)";

# Getting values from parsed mail
my $Sender = $entity->get( 'From' );
$Sender ||=  $entity->get( 'Reply-To' );
my $Message_ID = $entity->get( 'Message-Id' );

DealWithError (" *** Cant find Sender-adress in sent mail ! ***" ) unless defined( $Sender );
chomp( $Sender );
chomp( $Message_ID );

print STDERR "Dealing with the sender $Sender\n";

ConnectToDatabase();

my $SenderShort = $Sender;
$SenderShort =~ s/^.*?([a-zA-Z0-9_.-]+?\@[a-zA-Z0-9_.-]+\.[a-zA-Z0-9_.-]+).*$/$1/;

print STDERR "SenderShort is $SenderShort\n";

$SenderShort = findUser($SenderShort);

if (!defined($SenderShort)) {
  $SenderShort = $Sender;
  $SenderShort =~ s/^.*?([a-zA-Z0-9_.-]+?\@[a-zA-Z0-9_.-]+\.[a-zA-Z0-9_.-]+).*$/$1/;
}
print STDERR "The sendershort is now $SenderShort\n";

if (!defined($SenderShort)) {
  DealWithError("No such user $SenderShort exists.");
}

# get the user id
SendSQL("SELECT userid FROM profiles WHERE login_name = \'$SenderShort\';");
my $userid = FetchOneColumn();
if (!defined($userid)) {
  DealWithError("Userid not found for $SenderShort");
}

SendSQL("SELECT groupset FROM profiles WHERE userid=$userid;");
$::usergroupset = FetchOneColumn();
if (!$::usergroupset) { $::usergroupset = '0' }

my %ChangedBugs;

sub ChangeFields($$@) {
    my ($bug, $who, @changelist) = @_;

    my $id = $bug->{'bug_id'};

    my $error = 0;

    if ($bug->Collision) {
        DealWithWarning("Mid-air collision detected");
        return 0;
    }

    my $this;
    foreach $this (@changelist) {
        my ($fieldname,$settothis,$oldvalue,$newvalue) = @$this;

        $oldvalue = '' unless defined $oldvalue;
        next if $oldvalue eq $newvalue;

        if ($bug->CanChangeField($fieldname,$oldvalue,$newvalue) != 1) {
            DealWithWarning("You don't have permission to modify '$fieldname' of bug $id: ".$bug->{'error'});
            $error = 1;
            next;
        }
    }

    if ($error) {
        SendSQL("UNLOCK TABLES");
        return 0;
    }

    foreach (@changelist) {
        my ($fieldname,$settothis,$oldvalue,$newvalue) = @$_;

        $oldvalue = '' unless defined $oldvalue;
        next if $oldvalue eq $newvalue;

        SendSQL("UPDATE bugs SET $fieldname = ".SqlQuote($settothis)." WHERE bug_id = $id");
        my $fieldid = GetFieldID($fieldname);
        SendSQL("INSERT INTO bugs_activity " .
                "(bug_id,who,bug_when,fieldid,oldvalue,newvalue) VALUES " .
                "($id,$who,now(),".SqlQuote($fieldid).",".SqlQuote($oldvalue).",".SqlQuote($newvalue).")");

        DealWithMessage("Bug $id: Successfully changed '$fieldname' from '$oldvalue' to '$newvalue'");
    }

    SendSQL("UNLOCK TABLES");

    $ChangedBugs{$id} = 1;
    return 1;
}

sub DescCC {
    my $cclist = shift();

    return "" if ( $cclist->size() == 0 );

    return "Cc: " . $cclist->toString() . "\n";
}


sub DescDependencies {
    my ($id) = (@_);
    if (!Param("usedependencies")) {
        return "";
    }
    my $result = "";
    my $me = "blocked";
    my $target = "dependson";
    my $title = "BugsThisDependsOn";
    for (1..2) {
        SendSQL("select $target from dependencies where $me = $id order by $target");
        my @list;
        while (MoreSQLData()) {
            push(@list, FetchOneColumn());
        }
        if (@list) {
            my @verbose;
            my $count = 0;
            foreach my $i (@list) {
                SendSQL("select bug_status, resolution from bugs where bug_id = $i");
                my ($bug_status, $resolution) = (FetchSQLData());
                my $desc;
                if ($bug_status eq "NEW" || $bug_status eq "ASSIGNED" ||
                    $bug_status eq "REOPENED") {
                    $desc = "";
                } else {
                    $desc = "[$resolution]";
                }
                push(@verbose, $i . "$desc");
                $count++;
            }
            if ($count > 5) {
                $result .= "$title: Big list (more than 5) has been omitted\n";
            } else {
                $result .= "$title: " . join(', ', @verbose) . "\n";
            }
        }
        my $tmp = $me;
        $me = $target;
        $target = $tmp;
        $title = "OtherBugsDependingOnThis";
    }
    return $result;
}

sub GetBugText {
    my ($id) = (@_);
    undef %::bug;
    
    my @collist = ("bug_id", "product", "version", "op_sys", "op_sys_details",
                   "bug_status", "resolution", "priority", "bug_severity",
                   "assigned_to", "reporter", "bug_file_loc",
                   "short_desc", "component", "qa_contact", "target_milestone",
                   "status_whiteboard", "groupset", "externalcc");

    my $query = "select " . join(", ", @collist) .
        " from bugs where bug_id = $id";

    SendSQL($query);

    my @row;
    if (!(@row = FetchSQLData())) {
        return "";
    }
    foreach my $field (@collist) {
        $::bug{$field} = shift @row;
        if (!defined $::bug{$field}) {
            $::bug{$field} = "";
        }
    }

    $::bug{'assigned_to'} = DBID_to_name($::bug{'assigned_to'});
    $::bug{'reporter'} = DBID_to_name($::bug{'reporter'});
    my $qa_contact = "";
    my $target_milestone = "";
    my $status_whiteboard = "";
    if (Param('useqacontact') && $::bug{'qa_contact'} > 0) {
        $::bug{'qa_contact'} = DBID_to_name($::bug{'qa_contact'});
        $qa_contact = "QAContact: $::bug{'qa_contact'}\n";
    } else {
        $::bug{'qa_contact'} = "";
    }
    if (Param('usetargetmilestone') && $::bug{'target_milestone'} ne "") {
        $target_milestone = "TargetMilestone: $::bug{'target_milestone'}\n";
    }
    if (Param('usestatuswhiteboard') && $::bug{'status_whiteboard'} ne "") {
        $status_whiteboard = "StatusWhiteboard: $::bug{'status_whiteboard'}\n";
    }

    $::bug{'long_desc'} = GetLongDescriptionAsText($id);

    my $cclist = new RelationSet();
    $cclist->mergeFromDB("select who from cc where bug_id = $id");
    my @voterlist;
    SendSQL("select profiles.login_name from votes, profiles where votes.bug_id = $id and profiles.userid = votes.who");
    while (MoreSQLData()) {
        my $v = FetchOneColumn();
        push(@voterlist, $v);
    }
    $::bug{'cclist'} = $cclist->toString();
    $::bug{'voterlist'} = join(',', @voterlist);

    if (Param("prettyasciimail")) {
        $^A = "";
        my $temp = formline <<'END',$::bug{'short_desc'},$id,$::bug{'product'},$::bug{'bug_status'},$::bug{'version'},$::bug{'resolution'},$::bug{'component'},$::bug{'bug_severity'},$::bug{'priority'},$::bug{'op_sys'},$::bug{'op_sys_details'},$::bug{'assigned_to'},$::bug{'reporter'},$qa_contact,DescCC($cclist),$target_milestone,${status_whiteboard},$::bug{'bug_file_loc'},DescDependencies($id);
+============================================================================+
| @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< |
+----------------------------------------------------------------------------+
|        Bug #: @<<<<<<<<<<<                Product: @<<<<<<<<<<<<<<<<<<<<<< |
|       Status: @<<<<<<<<<<<<<<<<<<         Version: @<<<<<<<<<<<<<<<<<<<<<< |
|   Resolution: @<<<<<<<<<<<<<<<<<<       Component: @<<<<<<<<<<<<<<<<<<<<<< |
|     Severity: @<<<<<<<<<<<<<<<<<<        Priority: @<<<<<<<<<<<<<<<<<<<<<< |
+----------------------------------------------------------------------------+
| OS: @<<<<<<<<<<<  Details: @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< |
+----------------------------------------------------------------------------+
|  Assigned To: @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< |
|  Reported By: @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< |
|  ~QA Contact: ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< |
|  ~   CC list: ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< |
+----------------------------------------------------------------------------+
| ~  Milestone: ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< |
|~  Whiteboard: ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< |
|          URL: @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< |
|~Dependencies: ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< |
+============================================================================+
|                              DESCRIPTION                                   |
END

    my $prettymail = $^A . $::bug{'long_desc'};
        return $prettymail;


    } else {
        return "Bug\#: $id
Product: $::bug{'product'}
Version: $::bug{'version'}
OS: $::bug{'op_sys'}
OS Details: $::bug{'op_sys_details'}
Status: $::bug{'bug_status'}   
Resolution: $::bug{'resolution'}
Severity: $::bug{'bug_severity'}
Priority: $::bug{'priority'}
Component: $::bug{'component'}
AssignedTo: $::bug{'assigned_to'}                            
ReportedBy: $::bug{'reporter'}               
$qa_contact$target_milestone${status_whiteboard}URL: $::bug{'bug_file_loc'}
" . DescCC($cclist) . "Summary: $::bug{'short_desc'}
" . DescDependencies($id) . "
$::bug{'long_desc'}
";
}

}

sub SendReply($$$) {
    my ($sender, $subject, $reason) = @_;

    my $msg = new Mail::Send;
    $msg->to($sender);
    $msg->subject("Output of your command '$subject'");
    $msg->add("Reply-To", "bugmaster\@gnome.org");
    $msg->add("Errors-To", "unknown\@bugzilla.gnome.org");
    $msg->add("X-Loop", "bugzilla-daemon\@bugzilla.gnome.org");
    $msg->add("Precedence", "junk");

    my $fh = $msg->open ('sendmail');
    print $fh $reason;
    $fh->close;
}

sub DealWithError($) {
  my ($reason) = @_;
  SendReply($Sender, $Subject, $reason);
  die $reason;
}

sub DealWithWarning($) {
    my ($reason) = @_;
    push @Messages, $reason;
    $ErrorExit = 1;
}

sub DealWithMessage($) {
    my ($reason) = @_;
    push @Messages, $reason;
}

sub DoExit() {
    my $output = join "\n", @Messages;

    if ($output ne '') {
        $output .= "\n";
        SendReply($Sender, $Subject, $output);
        print STDERR $output;
    }
    if ($ErrorExit >= 0) {
        exit 1;
    } else {
        exit 0;
    }
}

sub SendMessage($$$) {
    my ($sender,$subject,$message) = @_;

    my $msg = new Mail::Send;
    $msg->to($sender);
    $msg->subject($subject);
    $msg->add("Reply-To", "bugmaster\@gnome.org");
    $msg->add("Errors-To", "unknown\@bugzilla.gnome.org");
    $msg->add("X-Loop", "bugzilla-daemon\@bugzilla.gnome.org");
    $msg->add("Precedence", "junk");

    my $fh = $msg->open ('sendmail');
    print $fh $message;
    $fh->close;
}

sub SendHelp($) {
    my ($sender) = @_;

    my $helptext;
    open HELPTEXT, "bugzilla_email_help.txt" or die
        "open (bugzilla_email_help.txt): $!";
    $helptext = join '', <HELPTEXT>;
    close HELPTEXT;

    SendMessage($sender, "Usage instructions for control\@bugzilla.gnome.org", $helptext);

    DealWithMessage("Sent help text to $SenderShort.");

    DoExit();
}

DealWithMessage("Processing request from $SenderShort.");

if ($Subject =~ /^\s*help\s*$/i) {
    SendHelp($Sender);
    exit;
}

my $bh = $entity->bodyhandle;
if (!$bh) {
    DealWithError("Can't get message body");
}

my (@lines, $line);
my $io = $bh->open("r");
while (defined($line = $io->getline)) {
    chop $line;
    next if $line =~ /^\#/;
    next if $line =~ /^\s*$/;
    last if $line =~ /^thanks\b/;

    if ($line =~ /^help/) {
        SendHelp($Sender);
        exit;
    }

    push @lines, $line;
}
$io->close;

my %changed_bugs;

 mainloop:
    while($#lines >= 0) {
        $line = shift @lines;

        DealWithMessage("\# " . $line);

        if ($line =~ /^confirm((?:\s+\d+)+)\s*$/) {
            my @bugs = split ' ', $1;

            if (!UserInGroup('canconfirm')) {
                DealWithWarning("You don't have permission to confirm a bug");
                next mainloop;
            }

            my $bug_id;
            foreach $bug_id (@bugs) {
                SendSQL("SELECT bug_status, everconfirmed FROM bugs WHERE bug_id = $bug_id;");
                my ($status, $everconfirmed) = (FetchSQLData());
                if (!defined $status) {
                    DealWithWarning("No such bug $bug_id");
                    next;
                } elsif ($status ne 'UNCONFIRMED') {
                    DealWithWarning("Bug $bug_id not in UNCONFIRMED state");
                    next;
                }

                SendSQL("UPDATE bugs SET bug_status = 'NEW', everconfirmed = 1 " .
                        "WHERE bug_id = $bug_id");
                my $fieldid = GetFieldID("bug_status");
                SendSQL("INSERT INTO bugs_activity " .
                        "(bug_id,who,bug_when,fieldid,oldvalue,newvalue) VALUES " .
                        "($bug_id,$userid,now(),$fieldid,'UNCONFIRMED','NEW')");
                if (!$everconfirmed) {
                    $fieldid = GetFieldID("everconfirmed");
                    SendSQL("INSERT INTO bugs_activity " .
                            "(bug_id,who,bug_when,fieldid,oldvalue,newvalue) VALUES " .
                            "($bug_id,$userid,now(),$fieldid,'0','1')");
                }

                $ChangedBugs{$bug_id} = 1;

                DealWithMessage("Successfully changed status of bug $bug_id to NEW");
            }
        } elsif ($line =~ /^accept((?:\s+\d+)+)\s*$/) {
            my @bugs = split ' ', $1;

            my $bug_id;
            foreach $bug_id (@bugs) {
                my $bug = new Bug $bug_id, $userid;

                if (defined $bug->{'error'}) {
                    DealWithWarning("Can't accept bug $bug_id: '".$bug->{'error'}."'");
                    next;
                }

                if (! &::IsOpenedState($bug->{'bug_status'})) {
                    DealWithWarning("Can't accept bug $bug_id: Not in open state");
                    next;
                }

                ChangeFields($bug,$userid,
                             ['assigned_to',$userid,$bug->{'assigned_to'},$SenderShort],
                             ['bug_status','ASSIGNED',$bug->{'bug_status'},'ASSIGNED']);
            }
        } elsif ($line =~ /^resolve\s+(fixed|wontfix|notabug|notgnome|incomplete|invalid)((?:\s+\d+)+)\s*$/i) {
            my ($resolution, @bugs) = ($1, split (' ', $2));

            $resolution =~ tr[a-z][A-Z];

            my $bug_id;
            foreach $bug_id (@bugs) {
                my $bug = new Bug $bug_id, $userid;

                if (defined $bug->{'error'}) {
                    DealWithWarning("Can't resolve bug $bug_id: '".$bug->{'error'}."'");
                    next;
                }

                if (! &::IsOpenedState($bug->{'bug_status'})) {
                    DealWithWarning("Can't resolve bug $bug_id: Not in open state");
                    next;
                }

                ChangeFields($bug,$userid,
                             ['bug_status','RESOLVED',$bug->{'bug_status'},'RESOLVED'],
                             ['resolution',$resolution,$bug->{'resolution'},$resolution]);
            }
        } elsif ($line =~ /^verified((?:\s+\d+)+)\s*$/i) {
            my @bugs = split (' ', $1);

            my $bug_id;
            foreach $bug_id (@bugs) {
                my $bug = new Bug $bug_id, $userid;

                if (defined $bug->{'error'}) {
                    DealWithWarning("Can't mark bug $bug_id as VERIFIED: '".$bug->{'error'}."'");
                    next;
                }

                if ($bug->{'bug_status'} ne 'RESOLVED') {
                    DealWithWarning("Can't mark bug $bug_id as VERIFIED: Not in RESOLVED state");
                    next;
                }

                ChangeFields($bug,$userid,
                             ['bug_status','VERIFIED',$bug->{'bug_status'},'VERIFIED']);
            }
        } elsif ($line =~ /^close((?:\s+\d+)+)\s*$/i) {
            my @bugs = split (' ', $1);

            my $bug_id;
            foreach $bug_id (@bugs) {
                my $bug = new Bug $bug_id, $userid;

                if (defined $bug->{'error'}) {
                    DealWithWarning("Can't close bug $bug_id: '".$bug->{'error'}."'");
                    next;
                }

                if (! ($bug->{'bug_status'} =~ /^RESOLVED|VERIFIED$/)) {
                    DealWithWarning("Can't close bug $bug_id: Must be either in RESOLVED or VERIFIED state");
                    next;
                }

                ChangeFields($bug,$userid,
                             ['bug_status','CLOSED',$bug->{'bug_status'},'CLOSED']);
            }
        } elsif ($line =~ /^reopen((?:\s+\d+)+)\s*$/) {
            my @bugs = split ' ', $1;

            my $bug_id;
            foreach $bug_id (@bugs) {
                my $bug = new Bug $bug_id, $userid;

                if (defined $bug->{'error'}) {
                    DealWithWarning("Can't reopen bug $bug_id: '".$bug->{'error'}."'");
                    next;
                }

                if (&::IsOpenedState($bug->{'bug_status'})) {
                    DealWithWarning("Can't reopen bug $bug_id: Already in open state");
                    next;
                }

                ChangeFields($bug,$userid,
                             ['bug_status','REOPENED',$bug->{'bug_status'},'REOPENED'],
                             ['resolution','',$bug->{'resolution'},'']);
            }
        } elsif ($line =~ /^comments((?:\s+\d+)+)\s*$/) {
            my @bugs = split ' ', $1;

            my $comment = '';
            while ($#lines >= 0) {
                last unless $lines[0] =~ /^\$/;
                $_ = shift @lines; s/^\$\s*//;
                $comment .= $_ . "\n";
            }
            $comment =~ s/^\s*//;
            $comment =~ s/\s*$//;

            my $bug_id;
            foreach $bug_id (@bugs) {
                my $bug = new Bug $bug_id, $userid;

                if (defined $bug->{'error'}) {
                    DealWithWarning("Can't reopen bug $bug_id: '".$bug->{'error'}."'");
                    next;
                }

                $bug->AppendComment($comment);
                $ChangedBugs{$bug_id} = 1;

                DealWithMessage("Added comment to bug $bug_id");
            }
        } elsif ($line =~ /^reassign\s+(\S+)((?:\s+\d+)+)\s*$/) {
            my ($newowner, @bugs) = ($1, split (' ', $2));

            my $bug_id;
            foreach $bug_id (@bugs) {
                my $bug = new Bug $bug_id, $userid;

                if (defined $bug->{'error'}) {
                    DealWithWarning("Can't reassign bug $bug_id: '".$bug->{'error'}."'");
                    next;
                }

                if (! &::IsOpenedState($bug->{'bug_status'})) {
                    DealWithWarning("Can't reassign bug $bug_id: Not in open state");
                    next;
                }

                my $realnewowner;
                if ($newowner =~ /^me|myself$/i) {
                    $realnewowner = $userid;
                } elsif ($newowner =~ /^default$/i) {
                    SendSQL("SELECT initialowner FROM components WHERE program=" .
                            SqlQuote($bug->{'product'}) . " AND value=" .
                            SqlQuote($bug->{'component'}));
                    $realnewowner = FetchOneColumn();
                } else {
                    $realnewowner = DBname_to_id($newowner);
                    if ($realnewowner == 0) {
                        DealWithWarning("Can't reassign bug $bug_id: No such user '$newowner'");
                        next;
                    }
                }

                my $newname = DBID_to_name($realnewowner);

                ChangeFields($bug,$userid,
                             ['assigned_to',$realnewowner,$bug->{'assigned_to'},$newname],
                             ['bug_status','ASSIGNED',$bug->{'bug_status'},'ASSIGNED']);
            }
        } elsif ($line =~ /^retitle\s+(\d+)\s+(.*)\s*$/) {
            my ($bug_id, $newtitle) = ($1, $2);

            my $bug = new Bug $bug_id, $userid;

            if (defined $bug->{'error'}) {
                DealWithWarning("Can't retitle bug $bug_id: '".$bug->{'error'}."'");
                next;
            }

            ChangeFields($bug,$userid,
                         ['short_desc',$newtitle,$bug->{'short_desc'},$newtitle]);
        } elsif ($line =~ /^move\s+(\S+)\s+(\S+)\s+(\S+)((?:\s+\d+)+)\s*$/) {
            my ($newproduct, $newcomponent, $newversion, @bugs) = ($1, $2, $3, split (' ', $4));

            my $bug_id;
            foreach $bug_id (@bugs) {
                my $bug = new Bug $bug_id, $userid;

                if (defined $bug->{'error'}) {
                    DealWithWarning("Can't move bug $bug_id: '".$bug->{'error'}."'");
                    next;
                }

                my ($movetoproduct, $movetocomponent, $movetoversion) = ($newproduct, $newcomponent, $newversion);
                my $newmilestone = $bug->{'target_milestone'};

                if ($newproduct eq '*') {
                    $movetoproduct = $bug->{'product'};
                } else {
                    # First check whether the product exists and whether it isn't closed for bug entry.
                    SendSQL("SELECT disallownew,defaultmilestone FROM products WHERE product=" .
                            SqlQuote($newproduct));
                    my ($disallownew,$defaultmilestone) = (FetchSQLData());
                    if (!defined $disallownew) {
                        DealWithWarning("Can't move bug $bug_id: No such product '$newproduct'");
                        next mainloop;
                    } elsif ($disallownew) {
                        DealWithWarning("Can't move bug $bug_id: Product '$newproduct' closed for bug entry");
                        next mainloop;
                    }

                    # Find out whether we need to change the target milestone.
                    SendSQL("SELECT value FROM milestones WHERE product=" .
                            SqlQuote($newproduct) . " AND value=" . SqlQuote($bug->{'target_milestone'}));
                    my $milestone = FetchOneColumn();
                    if (!defined $milestone) {
                        $newmilestone = $defaultmilestone;
                    }
                }

                $movetocomponent = $bug->{'component'} if $newcomponent eq '*';
                $movetoversion = $bug->{'version'} if $newversion eq '*';

                # Now check the component.
                SendSQL("SELECT value FROM components WHERE program = " .
                        SqlQuote($movetoproduct) . " AND value = " .
                        SqlQuote($movetocomponent));
                if (!defined FetchOneColumn()) {
                    DealWithWarning("Can't move bug $bug_id: Product '$movetoproduct' has no component '$movetocomponent'");
                    next mainloop;
                }

                # Now check the version.
                SendSQL("SELECT value FROM versions WHERE program = " .
                        SqlQuote($movetoproduct) . " AND value = " .
                        SqlQuote($movetoversion));
                if (!defined FetchOneColumn()) {
                    DealWithWarning("Can't move bug $bug_id: Product '$movetoproduct' has no version '$movetoversion'");
                    next mainloop;
                }

                ChangeFields($bug,$userid,
                             ['product',$movetoproduct,$bug->{'product'},$movetoproduct],
                             ['component',$movetocomponent,$bug->{'component'},$movetocomponent],
                             ['version',$movetoversion,$bug->{'version'},$movetoversion],
                             ['target_milestone',$newmilestone,$bug->{'target_milestone'},$newmilestone]);
            }
        } elsif ($line =~ /^severity\s+(blocker|critical|major|normal|minor|trivial|enhancement)((?:\s+\d+)+)\s*$/i) {
            my ($severity, @bugs) = ($1, split (' ', $2));

            $severity =~ tr[A-Z][a-z];

            my $bug_id;
            foreach $bug_id (@bugs) {
                my $bug = new Bug $bug_id, $userid;

                if (defined $bug->{'error'}) {
                    DealWithWarning("Can't change severity of bug $bug_id: '".$bug->{'error'}."'");
                    next;
                }

                ChangeFields($bug,$userid,
                             ['bug_severity',$severity,$bug->{'bug_severity'},$severity]);
            }
        } elsif ($line =~ /^priority\s+(urgent|high|normal|low)((?:\s+\d+)+)\s*$/i) {
            my ($priority, @bugs) = ($1, split (' ', $2));

            my ($a, $b) = (substr ($priority, 0, 1), substr ($priority, 1));
            $a =~ tr[a-z][A-Z]; $b =~ tr[A-Z][a-z]; $priority = $a . $b;

            my $bug_id;
            foreach $bug_id (@bugs) {
                my $bug = new Bug $bug_id, $userid;

                if (defined $bug->{'error'}) {
                    DealWithWarning("Can't change priority of bug $bug_id: '".$bug->{'error'}."'");
                    next;
                }

                ChangeFields($bug,$userid,
                             ['priority',$priority,$bug->{'priority'},$priority]);
            }
        } elsif ($line =~ /^cc((?:\s+\d+)+)\s*$/) {
            my @bugs = split ' ', $1;

            my $bug_id;
            foreach $bug_id (@bugs) {
                my $bug = new Bug $bug_id, $userid;

                if (defined $bug->{'error'}) {
                    DealWithWarning("Can't Cc: you on bug $bug_id: '".$bug->{'error'}."'");
                    next;
                }

                SendSQL("SELECT bug_id FROM cc WHERE bug_id = $bug_id AND who = $userid");
                if (defined FetchOneColumn()) {
                    DealWithWarning("You're already on the Cc: list of bug $bug_id");
                    next;
                }

                SendSQL("INSERT INTO cc (bug_id,who) VALUES ($bug_id,$userid)");
                DealWithMessage("Bug $bug_id: Added you to the Cc: list");
            }
        } elsif ($line =~ /^nocc((?:\s+\d+)+)\s*$/) {
            my @bugs = split ' ', $1;

            my $bug_id;
            foreach $bug_id (@bugs) {
                my $bug = new Bug $bug_id, $userid;

                if (defined $bug->{'error'}) {
                    DealWithWarning("Can't Cc: you on bug $bug_id: '".$bug->{'error'}."'");
                    next;
                }

                SendSQL("SELECT bug_id FROM cc WHERE bug_id = $bug_id AND who = $userid");
                if (!defined FetchOneColumn()) {
                    DealWithWarning("You are not on the Cc: list of bug $bug_id");
                    next;
                }

                SendSQL("DELETE FROM cc WHERE bug_id = $bug_id AND who = $userid");
                DealWithMessage("Bug $bug_id: Removed you from the Cc: list");
            }
        } elsif ($line =~ /^dup(?:licate)?\s+(\d+)((?:\s+\d+)+)\s*$/) {
            my ($dup_id, @bugs) = ($1, split (' ', $2));

            my $dup_bug = new Bug $dup_id, $userid;

            if (defined $dup_bug->{'error'}) {
                DealWithWarning("Can't close bugs as DUPLICATE of $dup_id: '".$dup_bug->{'error'}."'");
                next;
            }

            my $bug_id;
            foreach $bug_id (@bugs) {
                my $bug = new Bug $bug_id, $userid;

                if (defined $bug->{'error'}) {
                    DealWithWarning("Can't close bug $bug_id as DUPLICATE: '".$bug->{'error'}."'");
                    next;
                }

                if (! &::IsOpenedState($bug->{'bug_status'})) {
                    DealWithWarning("Can't close bug $bug_id as DUPLICATE: Not in open state");
                    next;
                }

                ChangeFields($bug,$userid,
                             ['bug_status','RESOLVED',$bug->{'bug_status'},'RESOLVED'],
                             ['resolution','DUPLICATE',$bug->{'resolution'},'DUPLICATE']);

                $bug->AppendComment(qq[*** This bug has been marked as a duplicate of $dup_id ***\n]);
                $dup_bug->AppendComment(qq[*** Bug $bug_id has been marked as a duplicate of this bug. ***\n]);
                $ChangedBugs{$bug_id} = 1;
                $ChangedBugs{$dup_id} = 1;

                DealWithMessage("Bug $bug_id: Closed as DUPLICATE of $dup_id");
                DealWithMessage("Bug $dup_id: Added comment about $bug_id being a DUPLICATE of this bug");
            }
        } elsif ($line =~ /^index\s+(\S+)\s+(\S+)\s*$/) {
            my ($product, $component) = ($1,$2);

            my $message = '';
            my $found = 0;
            my $query = "SELECT bug_id, bug_status, short_desc FROM bugs WHERE product = ".SqlQuote($product);

            if ($component ne "*") {
                $query .= " AND component = ".SqlQuote($component);
            }
            SendSQL($query);

            while(MoreSQLData()) {
                my ($bug_id, $state, $short_desc) = (FetchSQLData());
                $found = 1;

                next unless $state =~ /^(NEW|REOPENED|ASSIGNED|UNCONFIRMED)$/;

                $short_desc = '' unless defined $short_desc;
                $message .= sprintf "Bug %6d: %s\n", $bug_id, $short_desc;
            }

            if (length $message) {
                SendMessage($SenderShort,"Bug list for product '$product', component '$component'",$message);

                DealWithMessage("Sending index for product '$product', component '$component' in a separate mail to $SenderShort.");
            }

            if (!$found) {
                DealWithWarning("No such product '$product', component '$component'.");
            }
        } elsif ($line =~ /^send((?:\s+\d+)+)\s*$/) {
            my @bugs = split (' ', $1);

            my $bug_id;
            foreach $bug_id (@bugs) {
                my $bug = new Bug $bug_id, $userid;

                if (defined $bug->{'error'}) {
                    DealWithWarning("Can't send bug $bug_id: '".$bug->{'error'}."'");
                    next;
                }

                SendSQL("SELECT short_desc FROM bugs WHERE bug_id = $bug_id");
                my $short_desc = FetchSQLData();

                $short_desc = '' unless defined $short_desc;

                my $text = GetBugText($bug_id);

                if (defined $text) {
                    SendMessage($SenderShort,"[Bug $bug_id]: $short_desc",$text);

                    DealWithMessage("Sending bug $bug_id to $SenderShort.");
                }

            }
        } else {
            DealWithWarning("Unknown command '$line'");
        }

    }

foreach (keys %ChangedBugs) {
    system ("cd .. ; ./processmail $_ '$SenderShort'");
}

DoExit();

