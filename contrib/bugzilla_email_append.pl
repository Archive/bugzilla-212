#!/usr/local/perl/bin/perl -wT
# -*- Mode: perl; indent-tabs-mode: nil -*-

# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.

# The purpose of this script is to take an email message, which
# specifies a bugid and append it to the bug as part of the longdesc
# table

# Contributor : Seth M. Landsman <seth@dworkin.net>

# 03/15/00 : Initial version by SML
# 03/15/00 : processmail gets called

# Email subject must be of format :
# .* Bug ### .*
# replying to a typical bugzilla email should be valid

# TODO : 
# 1. better way to get the body text (I don't know what dump_entity() is 
#   actually doing

use diagnostics;
use strict;
use MIME::Parser;
require Mail::Address;
require Mail::Send;

$ENV{PATH} = '/bin:/usr/bin';

push @INC, "../."; # this script lives in contrib
push @INC, ".";
require "globals.pl";
require "BugzillaEmail.pm";
require "Bug.pm";

# Create a new MIME parser:
my $parser = new MIME::Parser;

my $Comment = "";
my $Subject = "(unknown subject)";

# Create and set the output directory:
# FIXME: There should be a $BUGZILLA_HOME variable (SML)
(-d "../data/mimedump-tmp") or mkdir "../data/mimedump-tmp",0755 or die "mkdir: $!";
(-w "../data/mimedump-tmp") or die "can't write to directory";

$parser->output_dir("../data/mimedump-tmp");
    
# Read the MIME message:
my $entity = $parser->read(\*STDIN) or die "couldn't parse MIME stream";
$entity->remove_sig(10);          # Removes the signature in the last 10 lines

# Getting values from parsed mail
my $Sender = $entity->get( 'From' );
$Sender ||=  $entity->get( 'Reply-To' );
my $Message_ID = $entity->get( 'Message-Id' );

DealWithError (" *** Cant find Sender-adress in sent mail ! ***" ) unless defined( $Sender );
chomp( $Sender );
chomp( $Message_ID );

#print "Dealing with the sender $Sender\n";

ConnectToDatabase();

my $SenderShort = $Sender;
$SenderShort =~ s/^.*?([a-zA-Z0-9_.-]+?\@[a-zA-Z0-9_.-]+\.[a-zA-Z0-9_.-]+).*$/$1/;

$SenderShort = findUser($SenderShort);

#print "SenderShort is $SenderShort\n";
if (!defined($SenderShort)) {
  $SenderShort = $Sender;
  $SenderShort =~ s/^.*?([a-zA-Z0-9_.-]+?\@[a-zA-Z0-9_.-]+\.[a-zA-Z0-9_.-]+).*$/$1/;
}
#print "The sendershort is now $SenderShort\n";

if (!defined($SenderShort)) {
  DealWithError("No such user $SenderShort exists.");
}

$Subject = $entity->get('Subject');
#print "The subject is $Subject\n";

my $To = $entity->get('To');
my $Cc = $entity->get('Cc');
my (@recipients, @addrs);

push @recipients, Mail::Address->parse($To) if defined $To;
push @recipients, Mail::Address->parse($Cc) if defined $Cc;
foreach (@recipients) {
    next unless $_->host =~ /^bugzilla(-test)?\.gnome\.org'$/
    push @addrs, $_->user;
}

my ($bugid, $action) = 0;
if ($#addrs == -1) {
    $bugid = ($Subject =~ /\[Bug ([\d]+)\]/);
    if ($bugid == 0) {
        DealWithError("Can't get bug \#ID from Subject line");
    }
} elsif ($#addrs > 0) {
    DealWithError("Can only specify one bug address");
} elsif ($addrs[0] =~ /^(\d+)(?:-(done|fixed|wontfix|notabug|notgnome|incomplete|invalid))?$/) {
    $bugid = $1; $action = $2;
} else {
    DealWithError("Invalid bug address `$addrs[0]'");
}
#print "The bugid is $bugid\n";

# make sure the bug exists

SendSQL("SELECT bug_id FROM bugs WHERE bug_id = $bugid;");
my $found_id = FetchOneColumn();
#print "Did we find the bug? $found_id-\n";
if (!defined($found_id)) {
  DealWithError("Bug $bugid does not exist");
}

# get the user id
SendSQL("SELECT userid FROM profiles WHERE login_name = \'$SenderShort\';");
my $userid = FetchOneColumn();
if (!defined($userid)) {
  DealWithError("Userid not found for $SenderShort");
}

# parse out the text of the message
dump_entity($entity);

# Get rid of the bug id
$Subject =~ s/\[Bug [\d]+\]//;
#my $Comment = "This is only a test ...";
my $Body = "Subject: " . $Subject . "\n" . $Comment;

if (defined $action) {
    $action =~ tr[a-z][A-Z];

    $action = 'FIXED' if $action eq 'DONE';

    my $bug = new Bug $found_id, $userid;

    if (defined $bug->{'error'}) {
        DealWithError("Can't resolve bug $found_id: '".$bug->{'error'}."'");
        exit;
    }

    if (! &::IsOpenedState($bug->{'bug_status'})) {
        DealWithError("Can't resolve bug $found_id: Not in open state");
        exit;
    }

    ChangeFields($bug,$userid,
                 ['bug_status','RESOLVED',$bug->{'bug_status'},'RESOLVED'],
                 ['resolution',$action,$bug->{'resolution'},$action]);
}

# shove it in the table
my $long_desc_query = "INSERT INTO longdescs SET bug_id=$found_id, who=$userid, bug_when=NOW(), thetext=" . SqlQuote($Body) . ";";
SendSQL($long_desc_query);

system("cd .. ; ./processmail $found_id '$SenderShort'");

sub ChangeFields {
    my ($bug, $who, @changelist) = @_;

    my $id = $bug->{'bug_id'};

    my $error = 0;

    if ($bug->Collision) {
        DealWithWarning("Mid-air collision detected");
        return 0;
    }

    my $this;
    foreach $this (@changelist) {
        my ($fieldname,$settothis,$oldvalue,$newvalue) = @$this;

        $oldvalue = '' unless defined $oldvalue;
        next if $oldvalue eq $newvalue;

        if ($bug->CanChangeField($fieldname,$oldvalue,$newvalue) != 1) {
            DealWithWarning("You don't have permission to modify '$fieldname' of bug $id: ".$bug->{'error'});
            $error = 1;
            next;
        }
    }

    if ($error) {
        SendSQL("UNLOCK TABLES");
        return 0;
    }

    foreach (@changelist) {
        my ($fieldname,$settothis,$oldvalue,$newvalue) = @$_;

        $oldvalue = '' unless defined $oldvalue;
        next if $oldvalue eq $newvalue;

        SendSQL("UPDATE bugs SET $fieldname = ".SqlQuote($settothis)." WHERE bug_id = $id");
        my $fieldid = GetFieldID($fieldname);
        SendSQL("INSERT INTO bugs_activity " .
                "(bug_id,who,bug_when,fieldid,oldvalue,newvalue) VALUES " .
                "($id,$who,now(),".SqlQuote($fieldid).",".SqlQuote($oldvalue).",".SqlQuote($newvalue).")");
    }

    SendSQL("UNLOCK TABLES");

    return 1;
}

sub SendReply {
    my ($sender, $subject, $reason) = @_;

    my $msg = new Mail::Send;
    $msg->to($sender);
    $msg->subject("Output of your command '$subject'");
    $msg->add("Reply-To", "bugmaster\@gnome.org");
    $msg->add("Errors-To", "unknown\@bugzilla.gnome.org");
    $msg->add("X-Loop", "bugzilla-daemon\@bugzilla.gnome.org");
    $msg->add("Precedence", "junk");

    my $fh = $msg->open ('sendmail');
    print $fh $reason;
    $fh->close;
}

sub DealWithError {
  my ($reason) = @_;
  SendReply($Sender, $Subject, $reason);
  die $reason;
}

# Yanking this wholesale from bug_email, 'cause I know this works.  I'll
#  figure out what it really does later
#------------------------------
#
# dump_entity ENTITY, NAME
#
# Recursive routine for parsing a mime coded mail.
# One mail may contain more than one mime blocks, which need to be
# handled. Therefore, this function is called recursively.
#
# It gets the for bugzilla important information from the mailbody and 
# stores them into the global attachment-list @attachments. The attachment-list
# is needed in storeAttachments.
#
sub dump_entity {
    my ($entity, $name) = @_;
    defined($name) or $name = "'anonymous'";
    my $IO;


    # Output the body:
    my @parts = $entity->parts;
    if (@parts) {                     # multipart...
	my $i;
	foreach $i (0 .. $#parts) {       # dump each part...
	    dump_entity($parts[$i], ("$name, part ".(1+$i)));
	}
    } else {                            # single part...	

	# Get MIME type, and display accordingly...
	my $msg_part = $entity->head->get( 'Content-Disposition' );
	
	$msg_part ||= ""; 

	my ($type, $subtype) = split('/', $entity->head->mime_type);
	my $body = $entity->bodyhandle;
	my ($data, $on_disk );

	if(  $msg_part =~ /^attachment/ ) {
	    # Attached File
	    my $des = $entity->head->get('Content-Description');
	    $des ||= "";

	    if( defined( $body->path )) { # Data is on disk
		$on_disk = 1;
		$data = $body->path;
		
	    } else {                      # Data is in core
		$on_disk = 0;
		$data = $body->as_string;
	    }
#	    push ( @attachments, [ $data, $entity->head->mime_type, $on_disk, $des ] );
	} else {
	    # Real Message
	    if ($type =~ /^(text|message)$/) {     # text: display it...
		if ($IO = $body->open("r")) {
		    $Comment .=  $_ while (defined($_ = $IO->getline));
		    $IO->close;
		} else {       # d'oh!
		    DealWithError ("$0: couldn't find/open '$name': $!");
		}
	    } else { DealWithError ("Oooops - no Body !"); }
	}
    }
}
