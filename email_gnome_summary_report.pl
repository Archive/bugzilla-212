#!/usr/bonsaitools/bin/perl -w
#
# email_gnome_summary_report.pl
#
# Send XML'ed weekly summary stats to the weekly gnome-summary ppl.
#
# Author: Wayne Schuller (k_wayne@linuxpower.org)
#
# TODO:
#

use diagnostics;
use strict;

require "globals.pl";
require "weekly-summary-utils.pl";

my $from = "k_wayne\@linuxpower.org";
my $to = "k_wayne\@linuxpower.org, gnome-summary\@gnome.org";
my $subject = "Weekly Gnome Bug Activity Report";
my $days = 7;      # Change this if the definition of week changes. 
my $product = "%"; # This report is for all products.
my $keyword = "%"; # Don't limit to any keyword.
my $number = 15;   # Show the top 15.

my $bugs_open = &print_total_bugs_on_bugzilla($keyword);
my $bugs_opened = &bugs_opened($product, $days, $keyword);
my $bugs_closed = &bugs_closed($product, $days, $keyword);

open SENDMAIL, "|/usr/lib/sendmail -t";
print SENDMAIL "From: $from\n";
print SENDMAIL "To: $to\n";
print SENDMAIL "Subject: $subject\n";
print SENDMAIL "<bug-stats>\n";
print SENDMAIL "<bug-modules open='$bugs_open' opened='$bugs_opened' closed='$bugs_closed'>\n";

# Passing the filehandle isn't tested well.
&print_product_bug_lists($number, $days, "XML", \*SENDMAIL, $keyword);

&print_bug_hunters_list($number, $days, "XML", \*SENDMAIL, $keyword);

print SENDMAIL "</bug-stats>\n";

close SENDMAIL;
