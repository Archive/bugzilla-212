#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
use strict;

require "CGI.pl";
require "globals.pl";

print "Content-type: text/html\n\n";

PutHeader ("Stack Traces and You",'');

print <<FIN;
<center><h1>Capturing Stack Traces</h1></center>
<p><b>What is a stack trace (backtrace)? Why is it important to
include a stack trace with my bug reports?</b></p>

<p>A stack trace is a list of function calls that leads to some point
in the program. Debugging tools like gdb or bug-buddy can get stack
traces from crashed applications so that developers can figure out
what went wrong.  By including a stack trace with your bug report, it
will be much easier for the developers to fix the reported
problem.</p>

<p><b>What does a stack trace look like?</b></p>

<p>A typical stack trace looks similar to the following:</p>

<blockquote>
<pre>[New Thread 8192 (LWP 15167)]

0x420ae169 in wait4 () from /lib/i686/libc.so.6
#0  0x420ae169 in wait4 () from /lib/i686/libc.so.6
#1  0x4212a2d0 in __DTOR_END__ () from /lib/i686/libc.so.6
#2  0x40842c63 in waitpid () from /lib/i686/libpthread.so.0
#3  0x4018cff5 in libgnomeui_module_info_get () from /usr/lib/libgnomeui-2.so.0
.
.
.
</pre>
</blockquote>


<p><b>How do I obtain a stack trace?</b></p>

<p>First of all, if you have compiled GNOME from source code (if you
don't know what that means, then you haven't done it) then please
first read this note about <a href="#compile"> compiling from source
with the appropriate flags</a>.</p>

<p>There are a few different ways to obtain a stack trace with
bug-buddy and gdb (<i>you only need to do one of these</i>):</p>

<ul>
  <li> Obtaining a stack trace using <a href="#bug-buddy-only">only
       Bug Buddy</a> </li>
  <li> Obtaining a stack trace from a <a href="#core-dump"> core
       dump</a> with GDB. </li>
  <li> Obtaining a stack trace using <a href="#gdb-only"> just GDB</a>
       </li>
  <li> Obtaining a stack trace using both <a href=
       "#bug-buddy-and-gdb"> Bug Buddy and GDB</a> </li>
</ul>

<p><b>I was told that my stack trace was not useful and that a stack
trace with debugging symbols was needed.  How do I get one of
those?</b></p>

<p>There are basically two ways.  Those two methods are:</p>

<ul>
  <li> Install an <a href="#debuginfo-rpm"> rpm package with debugging
       info</a> </li>
  <li> <a href="#compile">Compile</a> from source code with the
       appropriate flags.</li>
</ul>


<hr />


<a name="bug-buddy-only"><b>Obtaining a stack trace using only Bug
Buddy</b></a>

<p>If you use the GNOME Bug Report Tool, <a href=
"http://www.bug-buddy.org/">bug-buddy</a> to submit bugs, a stack
trace is usually included automatically (unfortunately, sometimes Bug
Buddy isn't able to obtain one).  For those on the most common linux
distributions, you should be able to install Bug Buddy from your
installation CDs that came with your distro.</p>


<a name="core-dump"><b>Obtaining a stack trace from a core dump</b></a>

<p>If the program that crashes leaves a core dump (which isn't very
common), you can use GDB to obtain a stack trace.  Core dumps are
saved in a file on your hard disk and are usually named something
along the lines of "core" or "core.3124".  To obtain a stack trace
from one of these files, run the following command:</p>

<blockquote>
<tt>gdb</tt> <i>name-of-program</i> <i>core-file-name</i>
</blockquote>

<p>Where <i>name-of-program</i> is the name of the program that crashed
(for example: <i>/usr/bin/gnome-panel</i>), and <i>core-file-name</i>
is the name of the core file that contains the core dump (for example:
<i>core.7812</i>).</p>

<p>Then, at the gdb prompt, type:</p>

<blockquote>
<tt>thread apply all bt</tt>
</blockquote>

<p>If that does not work (meaning that you don't get any output--this
may be the case in programs which are not multi-threaded), type
<tt>bt</tt> instead. If you still do not have any output, read this <a
href="#special-circumstances">note</a> about obtaining a stack trace
under special circumstances.  The output is the stack trace.  Cut and
paste all of it into a text file.</p>

<p>You can quit gdb by typing <tt>quit</tt>.</p>


<a name="gdb-only"><b>Obtaining a stack trace using just GDB</b></a>

<p>First, run the following command to start gdb:</p>

<blockquote>
<tt>gdb</tt> <i>name-of-program</i>
</blockquote>

<p>Where <i>name-of-program</i> is the name of the program that
crashed (for example: <i>/usr/bin/gnome-panel</i>).</p>

<p>Then, at the gdb prompt, type:</p>

<blockquote>
<tt>run</tt>
</blockquote>

<p>Once the program is running, reproduce the crash and go back to the
terminal where you ran gdb. The gdb prompt should be shown - if not,
hit Control+C to break into the debugger. At the gdb debugger prompt,
type:</p>

<blockquote>
<tt>thread apply all bt</tt>
</blockquote>

<p>If that does not work (meaning that you don't get any output--this
may be the case in programs which are not multi-threaded), type
<tt>bt</tt> instead. If you still do not have any output, read this <a
href="#special-circumstances">note</a> about obtaining a stack trace
under special circumstances.  The output is the stack trace.  Cut and
paste all of it into a text file.</p>

<p>You can quit gdb by typing <tt>quit</tt>.</p>

<a name="bug-buddy-and-gdb"><b>Obtaining a stack trace using both Bug
Buddy and GDB</b></a>

<p>When the program crashes, bug buddy should pops up and give an
error message similar to the following:</p>

<blockquote>
Application "gnome-panel" (process 12662) has crashed due to a fatal
error.
</blockquote>

<p>Do not close the Bug Buddy window.  When you see this message, use
the following command to start gdb:</p>

<blockquote>
<tt>gdb</tt> <i>name-of-program</i> <i>process-number</i>
</blockquote>

<p>Where <i>name-of-program</i> is the name of the program that
crashed along with the full path to where it is located (for example:
<i>/usr/bin/gnome-panel </i>; if you don't know the full path you can
use <i>`which gnome-panel`</i>--but be sure to include the backward
quote marks), and <i>process-number</i> is the process number that Bug
Buddy reported (for example, in this case it would be 12662).</p>

<p>Then, at the gdb prompt, type:</p>

<blockquote>
<tt>thread apply all bt</tt>
</blockquote>

<p>If that does not work (meaning that you don't get any output--this
may be the case in programs which are not multi-threaded), type
<tt>bt</tt> instead. If you still do not have any output, read this <a
href="#special-circumstances">note</a> about obtaining a stack trace
under special circumstances.  The output is the stack trace.  Cut and
paste all of it into a text file.</p>

<p>You can quit gdb by typing <tt>quit</tt>.</p>


<a name="special-circumstances"></a>

<p><b>Obtaining stack traces under special circumstances</b></p>

<p>If you do not get any output from gdb after typing <tt>thread apply
all bt</tt> or <tt>bt</tt>, it may be because the program is run as
root or as another user. In GNOME, this is the case when running
gnome-games. In such cases, you will need to be root in order to
capture a trace.  So, quit gdb, login as root, and then repeat the
steps to obtain a stack trace.</p>


<hr />

<a name="debuginfo-rpm"></a>

<p><b>Install an rpm package with debugging info</b></p>

<p>RedHat recently made it possible for users of their latest
distribution to install rpms that contained debugging info alongside
their standard rpms.  Hopefully the other distributions will follow
this lead soon.  So, those using RedHat 9 can obtain a stack trace by
installing the relevant debuginfo rpms.</p>

<a name="compile"></a>

<p><b>Compile from source code with the appropriate flags.</b></p>

<p>To get a stack trace with debugging symbols, the executable (and
libraries) must be compiled with debugging symbols.  (Note that
debugging symbols are created by default if the program is compiled
from CVS, and they are included in the snapshot builds.)  Some
optimization flags, such as <tt>-fomit-frame-pointer</tt>,
<tt>-O3</tt> (or higher), or processor specific optimizations can
render a stack trace useless.  Debugging symbols are turned on by
passing the <tt>-g</tt> flag to the gcc compiler.  So, if you compile
with custom <tt>CFLAGS</tt> values, please make sure that the compiler
is invoked with <tt>-g</tt>.</p>


FIN
PutFooter();

exit;
