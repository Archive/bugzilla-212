#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
#TODO:
#see also bug 87927
#start with more focused search- i.e., just in the specified component/product, then move to broader search if
# that fails
#if we don't get a result, we can try a different tack, like just searching for a single function name?
#when we upgrade to 2.16, this should also display what numbers something is a dup of it is a dup
#needs to ignore some functions like glog [see results for bugs 83738 and 86385 in particular]
#need to be able to pass it a stack trace or just function names to see if they already exist
#we might want to extract sigsuspends and treat them differently, since they are frequently in highly threaded apps where
# the next few frames aren't as relevant

#BUGS
#83904 is a dup of 78700 but not vice-versa
#'no symbol table info available' is not dealt with, should be

use diagnostics;
use strict;

require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";
require "find-traces.pl";

#set up the page
ConnectToDatabase(1);
GetVersionTable();

# Output appropriate HTTP response headers
print "Content-type: text/html\n\n";

my $bug_id = $::FORM{'id'};
my $custom_trace = $::FORM{'custom_trace'};
PutHeader("possible CRASH dups",'');

if(defined($bug_id) || defined($custom_trace)){
	print <<FIN;
	USE: simple-dup-finder.cgi?id=BUGNUMBER<br>
	BUGNUMBER is a bug with a stack trace- this doesn't work for non-crash bugs, and probably never will. :)<br>
	This page is <i>extremely</i> alpha right now. It is:
	<ul>
	<li>probably inaccurate
	<li>definitely too strict
	<li>probably too slow
	<li>doesn't handle any special cases
	</ul>
	That said, it's been useful to me :) If you have problems, suggestions, whatever... please, please add a comment to <a href="show_bug.cgi?id=87927">bug 87927</a> or email louie\@ximian.com or bugmaster\@gnome.org if you don't like bugzilla. I'm most interested in cases where it you know a duplicate exists and note that this page doesn't show it- any examples like that can only make the algorithm better. But other suggestions are welcome as well. <p>
	First five function calls in the bug:<p>
	<ol>
FIN

	my $text;
	if(defined($custom_trace)) {
		$text = $custom_trace;
	}
	else {
		$text = get_long_description_from_database($bug_id);
	}
	my @functions = get_traces_from_string($text);

	foreach my $func (@functions){
   	 print '<li>'.$func;
	}

	print"</ol>";

	#There weren't any functions found, which is probably an error :) 
	if($functions[0] eq ''){
	print<<FIN;
	No stack symbols were found in bug $bug_id. This may represent a bug in this page; if you believe there is a stack trace on the page, please email the bug number $bug_id to louie\@ximian.com, or add a comment to <a href="show_bug.cgi?id=87927">bug 87927.</a>.
FIN
	exit;
	}

	print<<FIN;
	<p>
	Bugs that contain all five function calls:<p>
FIN

   	 my @bug_data = get_duplicates_given_functions(@functions);

	foreach my $bug_ref (@bug_data){
	print '<li><a href=show_bug.cgi?id='.$$bug_ref{id}.'>'.$$bug_ref{id}.'</a> '.$$bug_ref{sta}.' '.$$bug_ref{des}.'<br>';
	}
}
else{
	print<<FIN
	1.) Bug Number:<br>
	<form action="simple-dup-finder-dev.cgi" method="get">
	<input type="text" name="id">
	<input type="submit" value="Go">
	</form>
	<p>
	2.) Paste Trace:<br>
	<form action="simple-dup-finder-dev.cgi" method="post">
	<textarea name="custom_trace" cols="80" rows="25" wrap></textarea>
	<input type="submit" value="Go">
	</form>
FIN
}
print<<FIN;
<p>
All bugs with valid strack traces should report <i>at least</i> themselves as duplicates of themselves. If they don't, that's a bug; please add a comment to <a href="show_bug.cgi?id=87927">bug 87927</a> or email louie\@ximian.com with the bug number that does <i>not</i> report itself as a duplicate of itself.
FIN

PutFooter();
