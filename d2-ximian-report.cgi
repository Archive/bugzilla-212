#!/usr/bonsaitools/bin/perl -w
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#

use diagnostics;
use strict;

require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";
require "g2-progress-utils.pl";

# Output appropriate HTTP response headers
print "Content-type: text/html\n\n";

ConnectToDatabase(1);

PutHeader("Ximian Sun team Bug Report");

# If they didn't tell us a time period we choose the last day.
my $days = 1; 

if (defined $::FORM{'days'}){
	$days = $::FORM{'days'};
}


print "note: because nautilus shows up across multiple people, the totals in the 'outstanding' column may be slightly misleading.<BR>";

print <<FIN; 
<table border=3 cellpadding=5>
    <tr>
    <td align=center bgcolor="#DDDDDD"><b>Developer</b></td>
    <td align=center bgcolor="#DDDDDD"><b>Fixed</b></td>
    <td align=center bgcolor="#DDDDDD"><b>Resolved</b></td>
    <td align=center bgcolor="#DDDDDD"><b>New</b></td>
    <td align=center bgcolor="#DDDDDD"><b>Outstanding</b></td>
    </tr>
FIN

if (defined $::FORM{'devel'}){
&print_table_row($::FORM{'devel'});
}

else{
&print_table_row('damon@ximian.com', 'glade', 'libgnomeprint', 'libgnomeprintui');
&print_table_row('dave@ximian.com','GConf', 'nautilus');
&print_table_row('federico@ximian.com','libgnomeui','libgnomecanvas','EOG','gedit');
&print_table_row('jacob@ximian.com', 'libgnome', 'libglade', 'xscreensaver','gnome-session');
&print_table_row('jody@gnome.org', 'control-center', 'gsearchtool', 'nautilus');
&print_table_row('michael@ximian.com', 'bonobo','nautilus');
&print_table_row('alex@ximian.com', 'printman','gnome-vfs');
}

PutFooter();


sub print_table_row {
    my ($devel, @prods) = @_;
print "<tr><td align=left><tt>".$devel."</tt></td>";
print "<td align=center>".&outgoing_fixes($days,$devel,'')."</td>";
print "<td align=center>".&outgoing_nonfix_resolutions($days,$devel,'')."</td>";
print "<td align=center>".&multiprod_incoming_bugs($days,@prods)."</td>";
print "<td align=center>".&multiprod_outstanding_bugs(@prods)."</td>";
print "</tr>";
}


