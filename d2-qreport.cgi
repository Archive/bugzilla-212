#!/usr/bonsaitools/bin/perl -w
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#

use diagnostics;
use strict;

require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";
require "g2-progress-utils.pl";

# Output appropriate HTTP response headers
print "Content-type: text/html\n\n";

ConnectToDatabase(1);

PutHeader("Ximian Sun team Bug Report");

# If they didn't tell us a time period we choose the last day.

print <<FIN; 
<table border=3 cellpadding=5>
    <tr>
    <td align=center bgcolor="#DDDDDD"><b>Developer</b></td>
    <td align=center bgcolor="#DDDDDD"><b>6/24-6/30</b></td>
    <td align=center bgcolor="#DDDDDD"><b>6/31-7/6</b></td>
    <td align=center bgcolor="#DDDDDD"><b>7/7-7/13</b></td>
    <td align=center bgcolor="#DDDDDD"><b>7/14-7/20</b></td>
    <td align=center bgcolor="#DDDDDD"><b>Outstanding</b></td>
    </tr>
FIN

if (defined $::FORM{'devel'}){
&print_table_row($::FORM{'devel'});
}

else{
&print_table_row('damon@ximian.com');
&print_table_row('dave@ximian.com');
&print_table_row('federico@ximian.com');
&print_table_row('jacob@ximian.com');
&print_table_row('jody@gnome.org');
&print_table_row('michael@ximian.com');
&print_table_row('alex@ximian.com');
&print_table_row('clahey@ximian.com');
}

print'</table>';

PutFooter();

sub print_table_row {
#ugh this is ugly; started listing without thinking...
my $week_0_start='2002-06-24';
my $week_0_stop='2002-06-30';
my $week_1_start='2002-06-31';
my $week_1_stop='2002-07-06';
my $week_2_start='2002-07-07';
my $week_2_stop='2002-07-13';
my $week_3_start='2002-07-14';
my $week_3_stop='2002-07-20';

    my ($devel) = @_;
    my $count;
    my $url;
    print "<tr><td align=left><tt><b>".$devel."</b></tt></td>";
    ($count,$url) = &outgoing_fixes_over_span($week_0_start,$week_0_stop,$devel);
    print "<td align=center><a href=\"http://bugzilla.gnome.org/buglist.cgi?bug_id=".$url."\">".$count."</a></td>";
    ($count,$url) = &outgoing_fixes_over_span($week_1_start,$week_1_stop,$devel);
    print "<td align=center><a href=\"http://bugzilla.gnome.org/buglist.cgi?bug_id=".$url."\">".$count."</a></td>";
    ($count,$url) = &outgoing_fixes_over_span($week_2_start,$week_2_stop,$devel);
    print "<td align=center><a href=\"http://bugzilla.gnome.org/buglist.cgi?bug_id=".$url."\">".$count."</a></td>";
    ($count,$url) = &outgoing_fixes_over_span($week_3_start,$week_3_stop,$devel);
    print "<td align=center><a href=\"http://bugzilla.gnome.org/buglist.cgi?bug_id=".$url."\">".$count."</a></td>";
    print "</tr>";
}


