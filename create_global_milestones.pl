#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is the Bugzilla Bug Tracking System.
#
# The Initial Developer of the Original Code is Netscape Communications
# Corporation. Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation. All
# Rights Reserved.
#
# Contributor(s): Terry Weissman <terry@mozilla.org>


# This is a script suitable for running once a day from a cron job.  It 
# looks at all the bugs, and sends whiny mail to anyone who has a bug 
# assigned to them that has status NEW that has not been touched for
# more than 7 days.

use diagnostics;
use strict;

require "globals.pl";

ConnectToDatabase();

SendSQL("select product from products");

my @products;

while (my($product) = FetchSQLData()) {
    push @products, $product;
}

my $prod;
foreach $prod (@products) {
    print "insert into products values(\'GNOME2.0\',\'$prod\',\'0\')";
#    SendSQL("insert into products values(\'GNOME2.0\',\'$product\',\'0\')");
#    SendSQL("insert into products values(\'GNOME2Beta\',\'$product\',\'0\')");
#    SendSQL("insert into products values(\'GNOME2RC1\',\'$product\',\'0\')");
#    SendSQL("insert into products values(\'GNOME2.x\',\'$product\',\'0\')");
}

# Make versioncache flush
unlink "data/versioncache";
