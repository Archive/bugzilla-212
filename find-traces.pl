# -*- Mode: perl; indent-tabs-mode: nil -*-
use diagnostics;
use strict;

=head1 get_traces_from_string

takes a string that may contain a stack trace. If it does contain
a viable stack trace, the first five useful functions are returned.

=cut

sub get_traces_from_string {
  my ($string) = @_;
  my @functions = ();
  my $function_index = -1;
  my @lines = split('\n', $string);

  # For a simplification, all of this could be done in one regexp. [Ben Frantzdale]
  # You volunteering to write it, Ben? :) [Luis 'not great with regexp' Villa]

  foreach my $line (@lines) {
  # Here, we try to ignore things before <signal handler> and/or killpg()
    if ( $line =~ /<signal handler called>|killpg|sigaction|sigsuspend|\(gdb\) bt|gnome_init|g_log/ ) {
      $function_index = 0; #Now we've started looking for real functions, or reset after reading g_log tops
    }  #here, we get only function names after the garbage
    elsif ( $line =~ /^#\d+ +0x[0-9A-F]+ in ((\w|::|_)+) \(/i &&
	    $function_index >= 0){
      $functions[$function_index]=$1;
      $function_index++;
    }
    # We stop after five functions are found:
    last if $function_index > 4;
  }

  #did we go all the way through without getting any frames?
  #if so, we've got a weird trace; we'll pick it up ourselves [since we take for granted there is actually a trace there]
  if ( $function_index == -1 ) {
      foreach my $line (@lines) {
          if ( $line =~ /^#\d+ +0x[0-9A-F]+ in ((\w|::|_)+) \(/i ){
              push(@functions, $1);
              $function_index++;
          }
          # We stop after five functions are found:
          last if $function_index > 3; # only go to three because we started at -1
      }
  }

  return @functions;
}





=head1 get_long_description_from_database

takes a bug number and returns the long description of the bug.

=cut

sub get_long_description_from_database {
  my ($bugnum) = @_;
  $bugnum =~ s/(\d+)/$1/;

#old query, can probably be deleted now that we do this more intelligently
#my $query = <<FIN;
#SELECT thetext
#FROM longdescs
#WHERE
#bug_id='$bugnum'
#AND
#(
#thetext LIKE '%signal handler called%'
#OR
#thetext LIKE '%sigaction%'
#OR
#thetext LIKE '%killpg%'
#OR
#thetext LIKE '%(gdb) bt%'
#or
#thetext LIKE '%Backtrace was generated from%'
#or
#thetext LIKE '%Debugging Information%'
#)
#FIN

#use the experimental regexp here ;)
my $query = <<FIN;
SELECT thetext
FROM longdescs
WHERE
bug_id='$bugnum'
AND
thetext REGEXP "#[[:digit:]]+ +0x[0-9A-F]+ in \([[:alnum:]]+\)"
FIN

  SendSQL ($query);

  my $result = "";
  while (my ($text) = FetchSQLData()) {
    $result .= $text;
  }
  return $result;
}



=head1 get_duplicates_given_functions

takes a list of functions to be found in stack traces and returns
a list of bugs which match.

=cut
sub get_duplicates_given_functions {
my @functions = @_ ;

#Ben had been using a left join here, but it didn't work for some reason so I took it out. [Luis]
#We tried to originally judge order by just directly searching using REGEXP but mysql is sloooow
# for that case

my $query = <<FIN;
SELECT
    bugs.bug_id,
    substring(bugs.bug_status,1,4),
    substring(bugs.resolution,1,4),
    substring(bugs.short_desc, 1, 60),
    product,
    thetext
FROM
    bugs, longdescs 
where
longdescs.bug_id = bugs.bug_id
AND (INSTR(LOWER(longdescs.thetext),LOWER('$functions[0]'))
AND INSTR(LOWER(longdescs.thetext),LOWER('$functions[1]'))
AND INSTR(LOWER(longdescs.thetext),LOWER('$functions[2]'))
AND INSTR(LOWER(longdescs.thetext),LOWER('$functions[3]'))
AND INSTR(LOWER(longdescs.thetext),LOWER('$functions[4]')))
ORDER BY bugs.bug_status, bugs.bug_id
FIN


  my $crap_counter=0;
  SendSQL ($query);
  my @bug_list = ();
  my %hash;
  my $regexp = join('.*', @functions);

  while ((@hash{'id', 'sta', 'res', 'des', 'product', 'thetext'}) = FetchSQLData() ) {
      my %new_hash = %hash;
    next unless $hash{thetext} =~ /$regexp/s; # Make sure we match in order.
    if($hash{res} =~ /dup/i) {
      # If it's resolved, find what it's a duplicate of. [Ben]
      # this is a cool idea but doesn't actually work, since 'thetext'
      #  in this context is the original stack trace report and the note
      #  about duplication is in another thetext field. [Luis]
      $hash{thetext} =~ /.*This bug has been marked as a duplicate of (\d+)/i;
      $hash{sta} = "DUP$1";
    }
    push(@bug_list, \%new_hash);
    }
  return @bug_list;
}

#this is total crack, but it makes the perl+apache stuff happy- don't delete
1;
