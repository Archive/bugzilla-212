#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
use strict;

require "CGI.pl";
require "globals.pl";

print "Content-type: text/html\n\n";

PutHeader ("Reports");

print <<FIN;
<h2>Generic GNOME reports</h2>
<ul>
<li><a href="weekly-bug-summary.html">Summary of bug activity for the last week </a> (<a href="weekly-bug-summary-gnome20.html">GNOMEVER2.0</a>, <a href="weekly-bug-summary-gnome22.html">GNOMEVER2.2</a>, <a href="weekly-bug-summary-gnome23.html">GNOMEVER2.3</a>, <a href="weekly-bug-summary-gnome24.html">GNOMEVER2.4</a>, <a href="weekly-bug-summary-gnome25.html">GNOMEVER2.5</a>)<br>
<li><a href="describekeywords.cgi">Show the different keywords available to Gnome bugzilla and what they mean.</a>.<br>
<!--
<li><a href="product-keyword-report.cgi">Open bugs listed by product and keyword.</a>.
<li><a href="product-target-report.cgi">Open bugs listed by product and target.</a>.
-->
<li><a href="mostfrequent.html">Most frequently reported bugs for all of Gnome Bugzilla.</a>.<br>
<li><a href="product-mostfrequent.cgi">Most frequently reported bugs by product.</a>.<br>
<li><a href="unconfirmed-bugs.cgi">Show products with the most UNCONFIRMED bugs. Good starting point for new bughunters looking to triage bugs.</a>.<br>
<li><a href="needinfo-updated.cgi">Show the NEEDINFO reports which have been updated (eg: given new info)</a>.<br>
</ul>
<h2>Reports Specific to the GNOME 2.x effort</h2>
<ul>
<li><a href="core-bugs-today.cgi">Bugs filed today</a> against components listed at <a href="http://developer.gnome.org/dotplan/modules/">dotplan</a> as core GNOME2 components.</li>
<li><a href="gnome-25-report.html">Open bugs with the GNOMEVER2.5 keyword</a> broken down by product, component, and developer.</li>
<li><a href="gnome-24-report.html">Open bugs with the GNOMEVER2.4 keyword</a> broken down by product, component, and developer.</li>
<li><a href="gnome-23-report.html">Open bugs with the GNOMEVER2.3 keyword</a> broken down by product, component, and developer.</li>
<li><a href="gnome-22-report.html">Open bugs with the GNOMEVER2.2 keyword</a> broken down by product, component, and developer.</li>
<li><a href="gnome-20-report.html">Open bugs with the GNOMEVER2.0 keyword</a> broken down by product, component, and developer.</li>
<li><a href="weekly-bug-summary-gnome25.html">Summary of bug activity for the last week (GNOMEVER2.5 keyword)</a>.<br>
<li><a href="weekly-bug-summary-gnome24.html">Summary of bug activity for the last week (GNOMEVER2.4 keyword)</a>.<br>
<li><a href="weekly-bug-summary-gnome23.html">Summary of bug activity for the last week (GNOMEVER2.3 keyword)</a>.<br>
<li><a href="weekly-bug-summary-gnome22.html">Summary of bug activity for the last week (GNOMEVER2.2 keyword)</a>.<br>
<li><a href="weekly-bug-summary-gnome20.html">Summary of bug activity for the last week (GNOMEVER2.0 keyword)</a>.<br>
</ul>
If you have questions or suggestions for new reports, email <a href="mailto:bugmaster\@gnome.org">the GNOME bugmasters.</a><br><br>
FIN
PutFooter();

exit;

